import React from 'react';
import PropTypes from 'prop-types';
import {
 Form, Icon, Input, Button
} from 'antd';
import { connect } from 'react-redux';
import Notifications from '../../components/notifications/notifications';
import { auth } from '../../actions';

const FormItem = Form.Item;

class Login extends React.Component {
  static propTypes = {
    form: PropTypes.instanceOf(Object),
    loading: PropTypes.bool,
    error: PropTypes.instanceOf(Object),
    fetchLogin: PropTypes.func
  };

  state = {
    showPassword: false,
    email: '',
    password: ''
  }

  componentDidMount() {
    // To disabled submit button at the beginning.
    const { form: { validateFields } } = this.props;
    validateFields();
  }

  viewPassToggle = () => {
    const { showPassword } = this.state;
    this.setState({
      showPassword: !showPassword
    });
  }

  handleChange = (e) => {
    this.setState({
      [e.target.name]: e.target.value
    });
  }

  hasErrors = (fieldsError) => {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
  }

  getErrorField = (fieldName) => {
    const { form: { isFieldTouched, getFieldError }, error } = this.props;
    const validateFields = isFieldTouched(fieldName) && getFieldError(fieldName);
    if (fieldName === 'email') {
      const validateAPI = error && 'Invalid email or password';
      return validateFields || validateAPI;
    }
    return validateFields;
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { form: { validateFields }, fetchLogin } = this.props;
    const {
      email,
      password
    } = this.state;
    validateFields(() => {
      const payload = {
        email: email.toLowerCase(),
        password
      };
      fetchLogin(payload);
    });
  };

  showNotif = () => {
    Notifications('error', 'email and password did not match');
  }

  render() {
    const { form: { getFieldsError, getFieldDecorator }, loading } = this.props;
    const { showPassword } = this.state;

    const view = (
      <div className="login-wrapper" style={{ backgroundSize: 'auto 100%' }}>
        <div className="isoLoginContentWrapper">
          <div className="isoLoginContent">
            <div className="isoLogoWrapper">
              <span style={{ color: 'rgb(245, 42, 43)' }}>
                Welcome
              </span>
            </div>
            <Form onSubmit={this.handleSubmit} className="form-login">
              <FormItem
                validateStatus={this.getErrorField('email') ? 'error' : ''}
                help={this.getErrorField('email') || ''}
              >
                {getFieldDecorator('email', {
                  rules: [{ required: true, message: 'Please input your Email!' }]
                })(
                  <Input
                    name="email"
                    onChange={this.handleChange}
                    prefix={
                      <Icon
                        type="mail"
                        style={{ color: 'rgba(0,0,0,.25)' }}
                      />
                    }
                    placeholder="Email"
                  />
                )}
              </FormItem>
              <FormItem
                validateStatus={this.getErrorField('password') ? 'error' : ''}
                help={this.getErrorField('password') || ''}
              >
                {getFieldDecorator('password', {
                  rules: [{ required: true, message: 'Please input your Password!' }]
                })(
                  <Input
                    name="password"
                    onChange={this.handleChange}
                    prefix={
                      <Icon
                        type="lock"
                        style={{ color: 'rgba(0,0,0,.25)' }}
                      />
                    }
                    type={showPassword ? 'text' : 'password'}
                    placeholder="Password"
                    addonAfter={
                      <Icon
                        type="eye"
                        onClick={this.viewPassToggle}
                        style={{ cursor: 'pointer' }}
                      />
                    }
                  />
                )}
              </FormItem>
              <FormItem style={{ float: 'right' }}>
                <Button
                  type="primary"
                  htmlType="submit"
                  loading={loading}
                  onClick={this.enterLoading}
                  disabled={this.hasErrors(getFieldsError())}
                  className="login-form-button"
                >
                  Login
                </Button>
              </FormItem>
            </Form>
          </div>
        </div>
      </div>
    );
    return (
      <React.Fragment>
        {view}
      </React.Fragment>
    );
  }
}

const mapStateToProps = (state) => {
  const {
    isLogin, loading, error
  } = state.auth;
  return {
    isLogin,
    loading,
    error
  };
};

const mapDispatchToProps = (dispatch) => {
  return {
    fetchLogin: data => dispatch(auth.fetchLogin(data))
  };
};

const WrappedNormalLoginForm = Form.create()(Login);

export default connect(mapStateToProps, mapDispatchToProps)(WrappedNormalLoginForm);
