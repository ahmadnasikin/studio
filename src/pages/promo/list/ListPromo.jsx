import React from 'react';
import {
  Button, Table, Icon, Divider, Modal, message, Tooltip
} from 'antd';
import momentTZ from 'moment-timezone';
import { Link } from 'react-router-dom';
import axios from '../../../utils/axios';
import config from '../../../../config';

const { confirm } = Modal;

class ListPromo extends React.Component {
  state = {
    data: [],
    pagination: {
      total: 0,
      current: 1,
      pages: 1,
      limit: 10
    },
    sort: null,
    order: 'ASC',
    loading: false
  }

  componentDidMount() {
    this.fetch();
  }

  handleTableChange = (pagination, filter, sorter) => {
    setTimeout(() => {
      const { pagination: pageState } = this.state;
      const pager = {
        ...pageState,
        current: pagination.current
      };
      const order = sorter.order === 'ascend' ? 'ASC' : 'DESC';
      this.setState({
        pagination: pager,
        sort: sorter.field,
        order
      },
      () => {
        this.fetch();
      });
    }, 400);
  }

  fetch = () => {
    const {
      pagination: { current, limit }, sort, order
    } = this.state;
    const params = {
      page: current,
      limit,
      sort,
      order
    };
    this.setState({ loading: true });
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}promozone/api/promo-zone`;
    axios({
      method: 'get',
      url: apiPath,
      params: {
        ...params
      },
      headers: { Authorization: token }
    }).then((response) => {
      const { pagination } = this.state;
      const { data: { data } } = response;
      pagination.total = response.data.data_size;
      pagination.pages = response.data.page_size;
      pagination.limit = response.data.page_size;
      const promo = data.map((dataPromo, index) => {
        return {
          key: dataPromo.id,
          id_promo: dataPromo.id_promo,
          promo_type: dataPromo.promo_type.name,
          promo_name: dataPromo.promo_name,
          category_name: dataPromo.category.name,
          created_at: momentTZ(dataPromo.created_at).tz('Asia/Jakarta').format('YYYY-MM-DD'),
          index: (index + 1) + ((pagination.current - 1) * pagination.limit)
        };
      });
      this.setState({
        loading: false,
        data: promo,
        pagination
      });
    }).catch((err) => {
      console.log(err); /* eslint-disable-line no-console */
    });
  }

  deletePromo = (id) => {
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}promozone/api/promo-zone/delete/${id}`;
    axios({
      method: 'get',
      url: apiPath,
      headers: { Authorization: token }
    }).then(({ data }) => {
      if (data.status === 200 && data.message === 'success') {
        this.success();
        this.fetch();
      } else {
        message.warning(data.error.message);
      }
    }).catch((err) => {
      const { data } = err.response;
      message.warning(data.message);
    });
  }

  showDeleteConfirm = (id) => {
    const { deletePromo } = this;
    confirm({
      title: 'Are you sure delete this promo?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        deletePromo(id);
      },
      onCancel: () => {}
    });
  }

  getPromoById = (id) => {
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}promozone/api/promo-zone/${id}`;
    return axios({
      method: 'get',
      url: apiPath,
      headers: { Authorization: token }
    });
  }

  success = () => {
    message.success('success delete promo');
  };

  error = () => {
    message.error('internal server error');
  };

  render() {
    const delStyle = {
      cursor: 'pointer',
      color: 'red'
    };
    const {
      data, pagination, loading
    } = this.state;
    const columns = [{
      title: '#',
      dataIndex: 'index'
    }, {
      title: 'ID Promo',
      dataIndex: 'id_promo',
      sorter: false
    }, {
      title: 'Promo Type',
      dataIndex: 'promo_type',
      sorter: false,
      render: (text, record) => (
        <Tooltip placement="right" title={text}>
          {`${text.slice(0, 12)}...`}
        </Tooltip>
      )
    }, {
      title: 'Promo Name',
      dataIndex: 'promo_name',
      sorter: false,
      render: (text, record) => (
        <Tooltip placement="right" title={text}>
          {`${text.slice(0, 12)}...`}
        </Tooltip>
      )
    }, {
      title: 'Category Name',
      dataIndex: 'category_name',
      sorter: false
    }, {
      title: 'Created Date',
      dataIndex: 'created_at',
      sorter: true
    }, {
      title: 'Action',
      dataIndex: 'action',
      render: (text, record) => (
        <span>
          <Link
            to={
            {
              pathname: `content/promo/detail/${record.key}`
            }
          }
            title="Detail"
          >
            <Icon type="eye" />
            <Divider type="vertical" />
          </Link>
          <Link
            to={
            {
              pathname: `/content/promo/edit/${record.key}`
            }
          }
            title="Edit"
          >
            <Icon type="edit" />
            <Divider type="vertical" />
          </Link>
          <span
            style={delStyle}
            title="Delete"
            role="presentation"
            tabIndex="-1"
            onClick={() => this.showDeleteConfirm(record.key)}
          >
            <Icon type="delete" />
          </span>
        </span>
      )
    }];
    return (
      <div>
        <h1>List Promo</h1>
        <div className="btn__wrapper">
          <div className="btn__wrapper--left">
          </div>
          <div className="btn__wrapper--right">
            <Link to="/promo/create-promo">
              <Button type="ghost" htmlType="submit" className="btn-success" icon="plus-circle">Add New</Button>
            </Link>
          </div>
        </div>
        <div className="table-wrapper">
          <Table
            className="table"
            columns={columns}
            dataSource={data}
            pagination={pagination}
            loading={loading}
            onChange={this.handleTableChange}
          />
        </div>
      </div>
    );
  }
}

export default ListPromo;
