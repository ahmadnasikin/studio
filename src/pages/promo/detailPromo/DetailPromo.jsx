import React from 'react';
import PropTypes from 'prop-types';
import {
  Spin,
  Button
} from 'antd';
import { Link } from 'react-router-dom';
import axios from '../../../utils/axios';
import config from '../../../../config';

class DetailPromo extends React.Component {
  state = {
    loading: true,
    data: {}
  }

  static propTypes = {
    match: PropTypes.instanceOf(Object)
  }

  componentWillMount() {
    this.getDetailPromo();
  }

  getDetailPromo = () => {
    const { match } = this.props;
    const { params: { id } } = match;
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}promozone/api/promo-zone/${id}`;
    axios({
      method: 'get',
      url: apiPath,
      headers: { Authorization: token }
    }).then((response) => {
      const { data: { data } } = response;
      this.setState({
        data,
        loading: false
      });
    })
    .catch((err) => {
      console.log('eror', err); /* eslint-disable-line no-console */
    });
  }

  render() {
    const {
    loading,
    data
  } = this.state;

    const loadingView = (
      <div><center><Spin /></center></div>
    );

    const view = (
      <div>
        <div className="iso__detail">
          <div className="iso__detail-heading"><p className="iso-heading-label">Detail Promo</p></div>
          <div
            className="iso__detail-info-wrapper"
          >
            <div
              style={{ padding: '24px 0' }}
              className="profile__wrapper--img-wrapper"
            >
              {
                data.hot_promo_image !== '' ?
                  <img alt="Featured" src={data.hot_promo_image} className="profile-img" /> :
                  <img alt="Featured" src="https://via.placeholder.com/60x60/" className="profile-img" />
              }
            </div>
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Promo Id</p>
            <p className="iso-info-detail">{data.id_promo}</p>
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Promo Type</p>
            {/* <p className="iso-info-detail">{data.promo_type.name}</p> */}
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Description</p>
            <p className="iso-info-detail">{data.description}</p>
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Category</p>
            {/* <p className="iso-info-detail">{data}</p> */}
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Status</p>
            {
              data.status !== 0 ? <p className="iso-info-detail">Publish</p> :
              <p className="iso-info-detail">Draft</p>
            }
          </div>
        </div>
        <Link
          to={{ pathname: '/promo' }}
          title="back"
        >
          <Button htmlType="button" className="btn-back">Back</Button>
        </Link>
      </div>
    );

    return (
      <div>
        { loading ? loadingView : view }
      </div>
    );
  }
}

export default DetailPromo;
