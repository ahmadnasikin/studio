/* eslint-disable semi */
import React from 'react';
import {
  Spin, Form, Input, Radio, Select, Button, Divider, message, InputNumber, TimePicker, DatePicker
} from 'antd';
import axios from 'axios';
import moment from 'moment';
import customAxios from '../../../utils/axios';
import config from '../../../../config';
import Notification from '../../../components/notifications/notifications';
import { formItemLayout, btn } from '../../../styles/component/formVariable';
import { typeList } from '../../../utils/typeList';
import { days } from '../../../utils/dayList';

const FormItem = Form.Item;
const { Option } = Select;

let errSubmit = false;
class CreatePromo extends React.Component {
  state = {
    loadingPage: true,
    loading: false,
    visible: false,
    fileExel: null,
    primaryImage: null,
    headerDetailImage: null,
    hotPromoImage: null,
    promoType: 1,
    promoTypeReward: null,
    merchantRedeem: null,
    maxUserGetReward: null,
    merchantSpecific: null,
    voucherAvailable: null,
    voucherPrice: null,
    revenueSharingBoost: null,
    revenueSharingMerchant: null,
    dateCampaignStart: '',
    dateCampaignEnd: '',
    timeActive: null,
    timerActiveStart: '',
    timerActiveEnd: '',
    maxRewardUser: 0,
    voucherGetAvailable: 0,
    maxRewardUserDate: '',
    cashbackNominal: 0,
    children: [],
    dayActive: [],
    categories: {}
    // dataForm: {}
  }

  componentWillMount() {
    this.getCategories();
    this.setLoading(false);
  }

  setLoading(status) {
    setTimeout(() => {
      this.setState({ loadingPage: status });
    }, 1000);
  }

  showModal = () => {
    const { visible } = this.state;
    this.handlePreviewSubmit();
    this.setState({
      visible: !visible
    });
  }

  handleOk = (e) => {
    const { visible } = this.state;
    this.handleSubmit(e);
    this.setState({
      visible: !visible
    });
  }

  handleCancel = (e) => {
    const { visible } = this.state;
    this.setState({
      visible: !visible
    });
  }

  handleReset = () => {
    const { form: { resetFields } } = this.props;
    resetFields();
  }

  handleExelUpload = name => (e) => {
    const file = e.target.files[0];
    const fileSize = parseInt(file.size, 10);
    if (fileSize > 500000) {
      Notification(
        'error',
        'Maximum file size is 500kb!',
      );
      return;
    }
    this.setState({
      [name]: file
    });
  }

  getCategories = () => {
    return axios.get(`${config.apiUrl}promozone/api/promo-zone/category`)
    .then((response) => {
      const { data } = response.data;
      const objectCategories = {};
      data.forEach((dataCategory) => { objectCategories[dataCategory.id] = dataCategory.name; });
      this.setState({ categories: objectCategories });
      this.getPromoById();
    });
  }

  maxUserGetReward = (userDaily, userDate) => {
    if (userDaily === null && userDate === null) {
      this.setState({ maxUserGetReward: '3' })
    } else if (userDaily !== null && userDate === null) {
      this.setState({ maxUserGetReward: '1' })
    } else {
      this.setState({ maxUserGetReward: '2' })
    }
  }

  getPromoById = () => {
    const { form } = this.props;
    const { match } = this.props;
    const { params: { id } } = match;
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}promozone/api/promo-zone/${id}`;
    customAxios({
      method: 'get',
      url: apiPath,
      headers: { Authorization: token }
    }).then((response) => {
      const { data: { data } } = response;
      const dataMaxRewardDaily = data.max_reward_user_daily;
      const dataMaxRewardate = data.max_reward_user_date;
      this.maxUserGetReward(dataMaxRewardDaily, dataMaxRewardate)
      const children2 = data.promo_merchant.map(dataMerchant => dataMerchant.qr_code);
      const dataCategory = data.category_id.toString();
      const dataStatus = data.status.toString();
      const dataHotPromo = data.show_hot_promo ? '1' : '0';
      const hotPromoPosition = data.hot_promo_position.toString();
      const merchantRedeemType = data.promo_merchant.length > 0 ? '1' : null;
      const daysActive = data.day_active !== null ? data.day_active.split(',').map(v => parseInt(v, 0)) : [];
      const times = (data.time_active_start && data.time_active_start) !== '' || null ? '2' : '1';
      form.setFieldsValue({
        promo_type_id: data.promo_type_id,
        category_id: dataCategory,
        status: dataStatus,
        show_hot_promo: dataHotPromo,
        hot_promo_position: hotPromoPosition,
        promo_name: data.promo_name,
        promo_validation: data.promo_validation,
        how_to_use: data.how_to_use,
        tnc: data.tnc,
        detail: data.detail,
        merchant_redeem_list: children2,
        merchant_specific: children2,
        cashback_coupon: data.cashback_coupon,
        cashback_percentage: data.cashback_percentage,
        max_reward_user_daily: data.max_reward_user_daily
      });
      this.setState({
        primaryImage: data.primary_image,
        headerDetailImage: data.image_header_detail,
        hotPromoImage: data.hot_promo_image,
        promoType: data.promo_type_id,
        dateCampaignStart: data.date_campaign_start,
        dateCampaignEnd: data.date_campaign_end,
        children: children2,
        merchantRedeem: merchantRedeemType,
        merchantSpecific: merchantRedeemType,
        voucherAvailable: data.buy_voucher_available,
        voucherPrice: data.voucher_price,
        revenueSharingBoost: data.revenue_sharing_boost,
        revenueSharingMerchant: data.revenue_sharing_merchant,
        dayActive: daysActive,
        promoTypeReward: data.reward_type_id !== null || '' ? data.reward_type_id.toString() : 0,
        timeActive: times,
        timerActiveStart: data.time_active_start,
        timerActiveEnd: data.time_active_end,
        maxRewardUser: data.max_reward_user,
        cashbackNominal: data.cashback_nominal,
        getVoucherAvailable: data.get_voucher_available,
        maxRewardUserDate: data.max_reward_user_date
      });
    }).catch((err) => {
      console.log(err) /* eslint no-console: "error" */
    });
  }

  handleFileUpload = name => (e) => {
    const file_ = e.target.files[0];
    const fileSize = parseInt(file_.size, 10);
    if (fileSize > 1000000) {
      Notification(
        'error',
        'Maximum file size is 1Mb!',
      );
      return;
    }
    const formData = new FormData();
    formData.append('image', file_);
    formData.append('imageName', 'promozone');
    const Auth = config.Authorization;
    const options = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: Auth
      }
    };
    axios.post(`${config.apiUrl}utils/upload`, formData, options)
    .then(({ data }) => {
      const urlImage = `${data.baseUrl}/${data.imageName}`;
      this.setState({
        [name]: urlImage
      });
      message.success(`${data.imageName} file uploaded successfully`);
    })
    .catch((err) => {
      Notification(
        'error',
        'Oops! Can\'t upload photo',
      );
    });
  }


  formatCurrency = (value) => {
    return value.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,');
  }

  typeChange = name => (value) => {
    this.setState({
      [name]: value
    });
  }

  handlePreviewSubmit = (e) => {
    const { form: { validateFields } } = this.props;
    validateFields((err, values) => {
      this.setState({
        dataForm: values
      });
    });
  }

  handleSubmit = (e) => {
    const { match } = this.props;
    const { params: { id } } = match;
    e.preventDefault();
    this.setState({
      loading: true
    });
    const { form: { validateFields } } = this.props;
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const {
      primaryImage,
      hotPromoImage,
      headerDetailImage,
      fileExel
    } = this.state;
    validateFields((err, values) => {
      if (!err) {
        const dataMerchant = !!values.merchant_redeem_list ? values.merchant_redeem_list : [''];
        const dayActive = !!values.day_active ? values.day_active : [''];
        const formData = new FormData();
        // promo magement
        formData.append('category_id', values.category_id);
        formData.append('date_campaign_end', values.date_campaign_end);
        formData.append('date_campaign_start', values.date_campaign_start);
        formData.append('detail', values.detail);
        formData.append('primary_image', primaryImage);
        formData.append('hot_promo_position', values.hot_promo_position);
        formData.append('how_to_use', values.how_to_use);
        formData.append('hot_promo_image', hotPromoImage);
        formData.append('promo_name', values.promo_name);
        formData.append('promo_type_id', values.promo_type_id);
        formData.append('promo_validation', values.promo_validation);
        formData.append('show_hot_promo', values.show_hot_promo);
        formData.append('status', values.status);
        formData.append('tnc', values.tnc);
        formData.append('image_header_detail', headerDetailImage);
        // buy voucher
        formData.append('buy_voucher_code', fileExel);
        dataMerchant.forEach((arr, index) => {
          formData.append(`merchant_redeem_list[${index}]`, arr);
        });
        formData.append('buy_voucher_available', values.buy_voucher_available);
        formData.append('voucher_price', values.voucher_price);
        formData.append('revenue_sharing_boost', values.revenue_sharing_boost);
        formData.append('revenue_sharing_merchant', values.revenue_sharing_merchant);
        // get voucher
        formData.append('target_user', values.target_user);
        formData.append('min_transaction', values.min_transaction);
        formData.append('reward_type_id', values.reward_type_id);
        formData.append('cashback_nominal', values.cashback_nominal);
        formData.append('cashback_coupon', values.cashback_coupon);
        formData.append('cashback_percentage', values.cashback_percentage);
        formData.append('time_active_start', moment(values.time_active_start).format('HH:mm:ss'));
        formData.append('time_active_end', moment(values.time_active_end).format('HH:mm:ss'));
        formData.append('max_reward_user', values.max_reward_user);
        formData.append('max_reward_user_daily', values.max_reward_user_daily);
        formData.append('max_reward_user_date', values.max_reward_user_date);
        formData.append('get_voucher_available', values.get_voucher_available);
        dayActive.forEach((arr, index) => {
          formData.append(`day_active[${index}]`, arr);
        });
        axios({
          method: 'post',
          url: `${config.apiUrl}promozone/api/promo-zone/update/${id}`,
          data: formData,
          headers: { Authorization: token }
        }).then(() => {
          this.setState({ loading: false });
          Notification('success', 'Success update promo');
        }).catch(() => {
          errSubmit = true;
          this.setState({ loading: false });
          Notification('error', 'Oops! Can\'t update promo');
        });
      } else {
        // errSubmit = true;
        this.setState({
          loading: false
        });
        Notification('error', 'Please Complete the Fields');
      }
    });
  }

  getErrorField = (fieldName) => {
    const { form: { isFieldTouched, getFieldError } } = this.props;
    let fieldTouched = isFieldTouched(fieldName);
    if (errSubmit) {
      fieldTouched = true;
    }
    const fieldErr = getFieldError(fieldName);
    return fieldTouched && fieldErr;
  }

  hasErrors = (fieldsError) => {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
  }

  handleFilterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;

  render() {
    const { form: { getFieldDecorator } } = this.props;
    const {
      loadingPage,
      loading,
      primaryImage,
      headerDetailImage,
      hotPromoImage,
      promoType,
      promoTypeReward,
      merchantRedeem,
      maxUserGetReward,
      merchantSpecific,
      timeActive,
      dateCampaignStart,
      dateCampaignEnd,
      children,
      categories,
      voucherAvailable,
      voucherPrice,
      revenueSharingBoost,
      revenueSharingMerchant,
      dayActive,
      timerActiveStart,
      timerActiveEnd,
      maxRewardUser,
      getVoucherAvailable,
      cashbackNominal,
      maxRewardUserDate
    } = this.state;

    const loadingView = (
      <div><center><Spin /></center></div>
    );

    const optionPromoTypeLists = Object.keys(typeList).map((value, index) => {
      return (
        <Option key={index} value={typeList[value].index}>{value}</Option>
      );
    });

    const optionDays = Object.keys(days).map((value, index) => {
      return (
        <Option key={index} value={days[value].index}>{value}</Option>
      );
    });

    const optionCategories = Object.keys(categories).map((value, index) => {
      return (
        <Option key={index} value={value}>{categories[value]}</Option>
      );
    });

    // const multipleMerchant = children.forEach((data, index) => {
    //   // children.push(<Option key={index}>{data}</Option>);
    // });

    const inputCashBackNominal = (
      <FormItem
        className="v-center"
        validateStatus={this.getErrorField('cashback_nominal') ? 'error' : ''}
        help={this.getErrorField('cashback_nominal') || ''}
        label="CashBack Nominal"
        {...formItemLayout}
      >
        {getFieldDecorator('cashback_nominal', {
          rules: [{ required: false, message: 'Field required!' }],
          initialValue: cashbackNominal
        })(
          <Input
            style={{ width: '40%' }}
            min={0}
            name="cashback_nominal"
            placeholder="Rp xxx"
          />
        )}
      </FormItem>
    );

    const inputCashBackCoupon = (
      <FormItem
        className="v-center"
        validateStatus={this.getErrorField('cashback_coupon') ? 'error' : ''}
        help={this.getErrorField('cashback_coupon') || ''}
        label="CashBack Coupon"
        {...formItemLayout}
      >
        {getFieldDecorator('cashback_coupon', {
          rules: [{ required: false, message: 'Field required!' }]
        })(
          <Input
            style={{ width: '40%' }}
            min={0}
            name="cashback_coupon"
            placeholder="Coupon"
          />
        )}
      </FormItem>
    );

    const inputCashBackPresentage = (
      <FormItem
        className="v-center"
        validateStatus={this.getErrorField('cashback_percentage') ? 'error' : ''}
        help={this.getErrorField('cashback_percentage') || ''}
        label="CashBack Persentage"
        {...formItemLayout}
      >
        {getFieldDecorator('cashback_percentage', {
          rules: [{ required: false, message: 'Field required!' }]
        })(
          <Input
            style={{ width: '40%' }}
            min={0}
            name="cashback_nominal"
            placeholder=""
          />
        )}
      </FormItem>
    );

    const inputMerchantRedeem = (
      <FormItem
        className="v-center"
        validateStatus={this.getErrorField('Merchant Redeem List') ? 'error' : ''}
        help={this.getErrorField('Merchant Redeem List') || ''}
        label="Input Multiple Merchants Boost"
        {...formItemLayout}
      >
        {getFieldDecorator('merchant_redeem_list', {
          rules: [{ type: 'array', required: false, message: 'Field required' }],
          defaultValue: []
        })(
          <Select
            mode="tags"
            style={{ width: '100%' }}
            min={0}
            name="merchant_redeem_list"
            placeholder="Enter input Merchant"
          />
        )}
      </FormItem>
    );

    const inputMerchantSpecific = (
      <FormItem
        className="v-center"
        validateStatus={this.getErrorField('Merchant Specific') ? 'error' : ''}
        help={this.getErrorField('Merchant Specific') || ''}
        label="Input Merchant Specific"
        {...formItemLayout}
      >
        {getFieldDecorator('merchant_specific', {
          rules: [{ type: 'array', required: false, message: 'Field required' }],
          initialValue: []
        })(
          <Select
            mode="tags"
            style={{ width: '100%' }}
            min={0}
            name="merchant_specific"
            placeholder="Enter input Merchant"
          />
        )}
      </FormItem>
    );

    const inputTimeActive = (
      <FormItem
        className="v-center"
        validateStatus={this.getErrorField('Time Active Start') ? 'error' : ''}
        help={this.getErrorField('Time Active Start') || ''}
        label="Range Time"
        {...formItemLayout}
      >
        {getFieldDecorator('time_active_start', {
          rules: [{ required: false, message: 'Field required!' }],
          initialValue: moment(timerActiveStart, 'HH:mm:ss')
        })(
          <TimePicker
            placeholder="Time Active Start"
            style={{ width: '40%' }}
          />
        )}
        {getFieldDecorator('time_active_end', {
          rules: [{ required: false, message: 'Field required!' }],
          initialValue: moment(timerActiveEnd, 'HH:mm:ss')
        })(
          <TimePicker
            placeholder="Time Active End"
            style={{ width: '40%', marginLeft: 6 }}
          />
        )}
      </FormItem>
    );

    const inputUserPerDay = (
      <FormItem
        className="v-center"
        validateStatus={this.getErrorField('Max Reward Per Day') ? 'error' : ''}
        help={this.getErrorField('Max Reward Per Day') || ''}
        label="Max Reward Per Day"
        {...formItemLayout}
      >
        {getFieldDecorator('max_reward_user_daily', {
          rules: [{ required: false, message: 'Field required!' }]
        })(
          <InputNumber
            style={{ width: '40%' }}
            type="number"
          />
        )}
      </FormItem>
    );

    const inputUserOnDate = (
      <FormItem
        className="v-center"
        validateStatus={this.getErrorField('max_reward_user_daily') ? 'error' : ''}
        help={this.getErrorField('max_reward_user_daily') || ''}
        label="User Daily"
        {...formItemLayout}
      >
        {getFieldDecorator('max_reward_user_daily', {
          rules: [{ required: false, message: 'Field required!' }]
        })(
          <InputNumber
            style={{ width: '40%' }}
            type="number"
          />
        )}
        {getFieldDecorator('max_reward_user_date', {
          rules: [{ required: false, message: 'Field required!' }],
          initialValue: moment(maxRewardUserDate, 'YYYY/MM/DD')
        })(
          <DatePicker
            placeholder="Date"
            style={{ marginLeft: 6 }}
          />
        )}
      </FormItem>
    );

    const view = (
      <div>
        <Divider>Promo Management</Divider>
        <Form onSubmit={this.handleSubmit}>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('promo_type_id') ? 'error' : ''}
            help={this.getErrorField('promo_type_id') || ''}
            label="Promo Type"
            {...formItemLayout}
          >
            {getFieldDecorator('promo_type_id', {
              rules: [{ required: true, message: 'Field required!' }],
              initialValue: 1
            })(
              <Select
                showSearch
                style={{ width: '50%' }}
                name="promoType"
                placeholder="Select Promo Type"
                optionFilterProp="children"
                filterOption={this.handleFilterOption}
                onChange={this.typeChange('promoType')}
              >
                {optionPromoTypeLists}
              </Select>
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('primaryImage') ? 'error' : ''}
            help={this.getErrorField('primaryImage') || ''}
            label="Primary Image"
            hasFeedback
            extra={primaryImage === null ? 'No photo available' : primaryImage}
            {...formItemLayout}
          >
            {getFieldDecorator('primaryImage', {
              rules: [{ required: false, message: 'Field required!' }]
            })(<Input
              type="file"
              size="large"
              name="primaryImage"
              id="primaryImage"
              placeholder="Image URL"
              onChange={this.handleFileUpload('primaryImage')}
            />)}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('headerDetailImage') ? 'error' : ''}
            help={this.getErrorField('headerDetailImage') || ''}
            label="Header Detail Image"
            hasFeedback
            extra={headerDetailImage === null ? 'No photo available' : headerDetailImage}
            {...formItemLayout}
          >
            {getFieldDecorator('headerDetailImage', {
              rules: [{ required: false, message: 'Field required!' }]
            })(<Input
              type="file"
              size="large"
              name="headerDetailImage"
              id="headerDetailImage"
              placeholder="Image URL"
              onChange={this.handleFileUpload('headerDetailImage')}
            />)}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('status') ? 'error' : ''}
            help={this.getErrorField('status') || ''}
            label="Status"
            {...formItemLayout}
          >
            {getFieldDecorator('status', {
              rules: [{ required: true, message: 'Field required!' }],
              defaultValue:  0
            })(
              <Radio.Group>
                <Radio.Button value="0">Draft</Radio.Button>
                <Radio.Button value="1">Publish</Radio.Button>
              </Radio.Group>
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('category_id') ? 'error' : ''}
            help={this.getErrorField('category_id') || ''}
            label="Categories"
            {...formItemLayout}
          >
            {getFieldDecorator('category_id', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Select
                showSearch
                style={{ width: '50%' }}
                name="promoType"
                placeholder="Select Categories"
                optionFilterProp="children"
              >
                {optionCategories}
              </Select>
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('date_campaign_start') ? 'error' : ''}
            help={this.getErrorField('date_campaign_start') || ''}
            label="Date Campaign"
            {...formItemLayout}
          >
            {getFieldDecorator('date_campaign_start', {
              rules: [{ required: false, message: 'Field required!' }],
              initialValue: moment(dateCampaignStart, 'YYYY/MM/DD')
            })(
              <DatePicker />
            )}
            {getFieldDecorator('date_campaign_end', {
              rules: [{ required: false, message: 'Field required!' }],
              initialValue: moment(dateCampaignEnd, 'YYYY-MM-DD')
            })(
              <DatePicker
                placeholder="End End"
                style={{ marginLeft: 6 }}
              />
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('show_hot_promo') ? 'error' : ''}
            help={this.getErrorField('show_hot_promo') || ''}
            label="Hot in Hot Promo"
            {...formItemLayout}
          >
            {getFieldDecorator('show_hot_promo', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Radio.Group>
                <Radio.Button value="0">No</Radio.Button>
                <Radio.Button value="1">Yes</Radio.Button>
              </Radio.Group>
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('hotPromoImage') ? 'error' : ''}
            help={this.getErrorField('hotPromoImage') || ''}
            label="Hot Promo Image"
            hasFeedback
            extra={hotPromoImage === null ? 'No photo available' : hotPromoImage}
            {...formItemLayout}
          >
            {getFieldDecorator('hot_promo_image', {
              rules: [{ required: false, message: 'Field required!' }]
            })(<Input
              type="file"
              size="large"
              name="hotPromoImage"
              id="headerDetailImage"
              placeholder="Image URL"
              onChange={this.handleFileUpload('hotPromoImage')}
            />)}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('hot_promo_position') ? 'error' : ''}
            help={this.getErrorField('hot_promo_position') || ''}
            label="Hot Promo Position"
            {...formItemLayout}
          >
            {getFieldDecorator('hot_promo_position', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Radio.Group>
                <Radio value="1">1</Radio>
                <Radio value="2">2</Radio>
                <Radio value="3">3</Radio>
                <Radio value="4">4</Radio>
                <Radio value="5">5</Radio>
                <Radio value="6">6</Radio>
                <Radio value="7">7</Radio>
                <Radio value="8">8</Radio>
                <Radio value="9">9</Radio>
                <Radio value="10">10</Radio>
              </Radio.Group>
            )}
          </FormItem>
          <Divider>Promo Info</Divider>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('promo_name') ? 'error' : ''}
            help={this.getErrorField('promo_name') || ''}
            label="Promo Name"
            {...formItemLayout}
          >
            {getFieldDecorator('promo_name', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Input
                name="promo_name"
                placeholder="Enter Promo Name"
              />
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('promo_validation') ? 'error' : ''}
            help={this.getErrorField('promo_validation') || ''}
            label="Promo Validation"
            {...formItemLayout}
          >
            {getFieldDecorator('promo_validation', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Input
                name="promo_validation"
                placeholder="Promo Validation"
              />
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('detail') ? 'error' : ''}
            help={this.getErrorField('detail') || ''}
            label="Detail"
            {...formItemLayout}
          >
            {getFieldDecorator('detail', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Input.TextArea
                name="detail"
                placeholder="Enter the Detail"
              />
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('how_to_use') ? 'error' : ''}
            help={this.getErrorField('how_to_use') || ''}
            label="How To Use"
            {...formItemLayout}
          >
            {getFieldDecorator('how_to_use', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Input.TextArea
                name="how_to_use"
                placeholder="How to Use"
              />
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('tnc') ? 'error' : ''}
            help={this.getErrorField('tnc') || ''}
            label="Term & Condition"
            {...formItemLayout}
          >
            {getFieldDecorator('tnc', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Input.TextArea
                name="tnc"
                placeholder="Term & Condition"
              />
            )}
          </FormItem>
          {promoType !== 2 ? null :
          <React.Fragment>
            <Divider>Promo Type: Buy Voucher</Divider>
            <FormItem
              className="v-center"
              validateStatus={this.getErrorField('Merchant redeem') ? 'error' : ''}
              help={this.getErrorField('Merchant redeem') || ''}
              label="Merchant Redeem List"
              {...formItemLayout}
            >
              <Select
                style={{ width: '40%' }}
                showSearch
                placeholder="Please Select Type"
                optionFilterProp="children"
                name="merchantRedeem"
                onChange={this.typeChange('merchantRedeem')}
                filterOption={this.handleFilterOption}
                value={merchantRedeem}
              >
                <Option value="1">Multiple Merchants</Option>
                <Option value="2">Non Merchant</Option>
              </Select>
            </FormItem>
            { merchantRedeem === '1' && inputMerchantRedeem }
            <FormItem
              validateStatus={this.getErrorField('upload_file') ? 'error' : ''}
              help={this.getErrorField('upload_file') || ''}
              label="Upload File Voucher Code"
              extra="maximun size 500kb"
              hasFeedback
              style={{ marginBottom: 8 }}
              {...formItemLayout}
            >
              {getFieldDecorator('upload_file', {
                rules: [{ required: false, message: 'Field required!' }]
              })(<Input
                type="file"
                size="large"
                name="upload_file"
                id="upload_file"
                placeholder="file URL"
                onChange={this.handleExelUpload('fileExel')}
              />)}
            </FormItem>
            <div style={{ marginLeft: '25%', marginBottom: '12px' }}>
              <a
                style={{ width: '40%' }}
                download="template_code"
                target="_blank"
                href="/assets/files/Template_Upload_Voucher_Code.xlsx">
                Download file template here
              </a>
            </div>
            <FormItem
              className="v-center"
              validateStatus={this.getErrorField('buy_voucher_available') ? 'error' : ''}
              help={this.getErrorField('buy_voucher_available') || ''}
              label="Voucher Available"
              {...formItemLayout}
            >
              {getFieldDecorator('buy_voucher_available', {
                rules: [{ required: true, message: 'Field required!' }],
                initialValue: voucherAvailable
              })(
                <Input
                  style={{ width: '30%' }}
                  placeholder="Voucher Available"
                  // type="number"
                />
              )}
            </FormItem>
            <FormItem
              className="v-center"
              validateStatus={this.getErrorField('voucher_price') ? 'error' : ''}
              help={this.getErrorField('voucher_price') || ''}
              label="Voucher Price"
              {...formItemLayout}
            >
              {getFieldDecorator('voucher_price', {
                rules: [{ required: true, message: 'Field required!' }],
                initialValue: voucherPrice
              })(
                <InputNumber
                  style={{ width: '30%' }}
                  min={0}
                  name="voucher_price"
                  placeholder="Voucher Price"
                  type="number"
                />
              )}
            </FormItem>
            <FormItem
              className="v-center"
              validateStatus={this.getErrorField('revenue_sharing_boost') ? 'error' : ''}
              help={this.getErrorField('revenue_sharing_boost') || ''}
              label="Revenue Sharing Boost"
              {...formItemLayout}
            >
              {getFieldDecorator('revenue_sharing_boost', {
                rules: [{ required: true, message: 'Field required!' }],
                initialValue: revenueSharingBoost
              })(
                <InputNumber
                  style={{ width: '30%' }}
                  min={0}
                  name="revenue_sharing_boost"
                  placeholder="Revenue Sharing Boost"
                  type="number"
                />
              )}
            </FormItem>
            <FormItem
              className="v-center"
              validateStatus={this.getErrorField('revenue_sharing_merchant') ? 'error' : ''}
              help={this.getErrorField('revenue_sharing_merchant') || ''}
              label="Revenue"
              {...formItemLayout}
            >
              {getFieldDecorator('revenue_sharing_merchant', {
                rules: [{ required: true, message: 'Field required!' }],
                initialValue: revenueSharingMerchant
              })(
                <InputNumber
                  style={{ width: '30%' }}
                  min={0}
                  name="revenue_sharing_merchant"
                  placeholder="Revenue"
                  type="number"
                />
              )}
            </FormItem>
          </React.Fragment>
          }
          {promoType === 3 ?
            <React.Fragment>
              <Divider>Promo Type: Get Voucher(Promo Code)</Divider>
              <FormItem
                className="v-center"
                validateStatus={this.getErrorField('target_user') ? 'error' : ''}
                help={this.getErrorField('target_user') || ''}
                label="Target User"
                {...formItemLayout}
              >
                {getFieldDecorator('target_user', {
                  rules: [{ required: false, message: 'Field required!' }],
                  initialValue: 'All User'
                })(
                  <Input
                    disabled
                    style={{ width: '40%' }}
                    min={0}
                    name="revenue_sharing_merchant"
                    placeholder="All User"
                  />
                )}
              </FormItem>
              <FormItem
                className="v-center"
                validateStatus={this.getErrorField('reward_type_id') ? 'error' : ''}
                help={this.getErrorField('reward_type_id') || ''}
                label="Reward Type"
                {...formItemLayout}
              >
                {getFieldDecorator('reward_type_id', {
                  rules: [{ required: false, message: 'Field required!' }],
                  initialValue: promoTypeReward
                })(
                  <Select
                    style={{ width: '40%' }}
                    showSearch
                    placeholder="Select Type Reward"
                    optionFilterProp="children"
                    name="promoTypeReward"
                    onChange={this.typeChange('promoTypeReward')}
                    filterOption={this.handleFilterOption}
                  >
                    <Option value="1">CashBack Rupiah</Option>
                    <Option value="2">Coupon</Option>
                    <Option value="3">Persentage</Option>
                  </Select>
                )}
              </FormItem>
              { promoTypeReward === '1' && inputCashBackNominal }
              { promoTypeReward === '2' && inputCashBackCoupon }
              { promoTypeReward === '3' && inputCashBackPresentage }
              <FormItem
                className="v-center"
                validateStatus={this.getErrorField('day_active') ? 'error' : ''}
                help={this.getErrorField('day_active') || ''}
                label="Day Active"
                {...formItemLayout}
              >
                {getFieldDecorator('day_active', {
                  rules: [{ type: 'array', required: false, message: 'Field required' }],
                  initialValue: dayActive
                })(
                  <Select
                    mode="multiple"
                    placeholder="Please select day"
                    filterOption={this.handleFilterOption}
                  >
                    {optionDays}
                  </Select>
                )}
              </FormItem>
              <FormItem
                className="v-center"
                validateStatus={this.getErrorField('time Active') ? 'error' : ''}
                help={this.getErrorField('time Active') || ''}
                label="Time Active"
                {...formItemLayout}
              >
                {getFieldDecorator('time Active', {
                  rules: [{ required: false, message: 'Field required!' }],
                  initialValue: timeActive
                })(
                  <Select
                    style={{ width: '40%' }}
                    showSearch
                    placeholder="Select Time"
                    optionFilterProp="children"
                    name="timeActive"
                    onChange={this.typeChange('timeActive')}
                    filterOption={this.handleFilterOption}
                  >
                    <Option value="1">All Time</Option>
                    <Option value="2">Range Time</Option>
                  </Select>
                )}
              </FormItem>
              { timeActive === '2' && inputTimeActive }
              <FormItem
                className="v-center"
                validateStatus={this.getErrorField('max_reward_user') ? 'error' : ''}
                help={this.getErrorField('max_reward_user') || ''}
                label="Max Reward User"
                {...formItemLayout}
              >
                {getFieldDecorator('max_reward_user', {
                  rules: [{ required: false, message: 'Field required!' }],
                  initialValue: maxRewardUser
                })(
                  <InputNumber
                    style={{ width: '40%' }}
                    min={0}
                    name="max_reward_user"
                    placeholder="Please Input Max User"
                    type="number"
                  />
                )}
              </FormItem>
              <FormItem
                className="v-center"
                validateStatus={this.getErrorField('Max User Get Reward') ? 'error' : ''}
                help={this.getErrorField('Max User Get Reward') || ''}
                label="Max User Get Reward"
                {...formItemLayout}
              >
                <Select
                  style={{ width: '40%' }}
                  showSearch
                  placeholder="Please Select Type"
                  optionFilterProp="children"
                  name="maxUserGetReward"
                  onChange={this.typeChange('maxUserGetReward')}
                  filterOption={this.handleFilterOption}
                  value={maxUserGetReward}
                >
                  <Option value="1">User Per Day</Option>
                  <Option value="2">User On Date</Option>
                  <Option value="3">No Limit Reward</Option>
                </Select>
              </FormItem>
              { maxUserGetReward === '1' && inputUserPerDay }
              { maxUserGetReward === '2' && inputUserOnDate }
              { maxUserGetReward === '3' && null }
              <FormItem
                className="v-center"
                validateStatus={this.getErrorField('Merchant Specific') ? 'error' : ''}
                help={this.getErrorField('Merchant Specific') || ''}
                label="Merchant Specific"
                {...formItemLayout}
              >
                <Select
                  style={{ width: '40%' }}
                  showSearch
                  placeholder="Please Select Type"
                  optionFilterProp="children"
                  name="merchantSpecific"
                  onChange={this.typeChange('merchantSpecific')}
                  filterOption={this.handleFilterOption}
                  value={merchantSpecific}
                >
                  <Option value="1">Multiple Merchants</Option>
                  <Option value="2">All Merchant Boost</Option>
                </Select>
              </FormItem>
              { merchantSpecific === '1' && inputMerchantRedeem }
              <FormItem
                className="v-center"
                validateStatus={this.getErrorField('get_voucher_available') ? 'error' : ''}
                help={this.getErrorField('get_voucher_available') || ''}
                label="Voucher Available"
                {...formItemLayout}
              >
                {getFieldDecorator('get_voucher_available', {
                  rules: [{ required: false, message: 'Field required!' }],
                  initialValue: getVoucherAvailable
                })(
                  <InputNumber
                    style={{ width: '30%' }}
                    min={0}
                    name="get_voucher_available"
                    placeholder="Voucher Available"
                    type="number"
                  />
                )}
                <span className="ant-form-text">pcs</span>
              </FormItem>
            </React.Fragment> : null
          }
          <FormItem {...btn}>
            <Button
              type="primary"
              htmlType="submit"
              loading={loading}
              className="update-form-button"
            >
              Submit
            </Button>
          </FormItem>
        </Form>
      </div>
    );

    return (
      <div>
        { loadingPage ? loadingView : view }
      </div>
    );
  }
}

const EnhancedForm = Form.create()(CreatePromo);

export default EnhancedForm;
