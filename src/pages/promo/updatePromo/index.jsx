import React from 'react';
import PropTypes from 'prop-types';
import { Spin, Form, Button } from 'antd';

import PromoManagament from './Forms/PromoManagement';
import PromoInfo from './Forms/PromoInfo';
import PromoBuyVoucher from './Forms/PromoBuyVoucher';
import PromoGetVoucher from './Forms/PromoGetVoucher';
import { btn } from '../../../styles/component/formVariable';

class UpdatePromoManagement extends React.Component {
  static propTypes = {
    form: PropTypes.instanceOf(Object)
  }

  state = {
    visibiltyPromoManagament: true,
    visibiltyPromoInfo: true,
    visibiltyPromoBuyVoucher: true,
    visibiltyPromoGetVoucher: true,
    loading: false,
    errSubmit: false
  }

  getErrorField = (fieldName) => {
    const { form: { isFieldTouched, getFieldError } } = this.props;
    const { errSubmit } = this.state;
    return (isFieldTouched(fieldName) || errSubmit) && getFieldError(fieldName);
  }

  handleSubmit = (e) => {
    e.preventDefault();
    const { form: { validateFields } } = this.props;
    validateFields((err, values) => {
      console.log(err, values);
      if (!err) {
        // do something
      } else {
        this.setState({ errSubmit: true });
      }
    });
  }

  render() {
    const {
      form
    } = this.props;
    const {
      visibiltyPromoManagament,
      visibiltyPromoInfo,
      visibiltyPromoBuyVoucher,
      visibiltyPromoGetVoucher,
      loading
    } = this.state;


    const loadingView = (<div><center><Spin /></center></div>);
    const view = (
      <div>
        <PromoManagament
          form={form}
          show={visibiltyPromoManagament}
          getErrorField={this.getErrorField}
        />
        <PromoInfo
          form={form}
          show={visibiltyPromoInfo}
          getErrorField={this.getErrorField}
        />
        <PromoBuyVoucher
          form={form}
          show={visibiltyPromoBuyVoucher}
          getErrorField={this.getErrorField}
        />
        <PromoGetVoucher
          form={form}
          show={visibiltyPromoGetVoucher}
          getErrorField={this.getErrorField}
        />
        <Form.Item {...btn}>
          <Button
            type="primary"
            htmlType="submit"
            loading={loading}
            className="update-form-button"
            onClick={this.handleSubmit}
          >
            Submit
          </Button>
        </Form.Item>
      </div>
    );

    return !!loading ? loadingView : view;
  }
}

const onValuesChanges = (props, changedValues, allValues) => {
  // console.log(props, changedValues, allValues);
};

export default Form.create({
  onValuesChange: onValuesChanges
})(UpdatePromoManagement);
