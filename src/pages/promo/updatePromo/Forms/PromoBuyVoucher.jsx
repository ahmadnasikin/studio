import React from 'react';
import {
  Divider,
  Form,
  Select,
  Input,
  InputNumber
} from 'antd';

import { formItemLayout } from '../../../../styles/component/formVariable';

class PromoBuyVoucher extends React.Component {
  state = {
    index: false,
    fileExcel: null
  }

  handleExelUpload = name => (e) => {
    const file = e.target.files[0];
    const fileSize = parseInt(file.size, 10);
    if (fileSize > 500000) {
      Notification(
        'error',
        'Maximum file size is 500kb!',
      );
      return;
    }
    this.setState({
      [name]: file
    });
  }

  render() {
    const {
      form,
      getErrorField
    } = this.props;

    return (
      <div>
        <Divider>Promo Type: Buy Voucher</Divider>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('Merchant redeem') ? 'error' : ''}
          help={getErrorField('Merchant redeem') || ''}
          label="Merchant Redeem List"
          {...formItemLayout}
        >
          <Select
            style={{ width: '40%' }}
            showSearch
            placeholder="Please Select Type"
            optionFilterProp="children"
            name="merchantRedeem"
          >
            <Select.Option value="1">Multiple Merchants</Select.Option>
            <Select.Option value="2">Non Merchant</Select.Option>
          </Select>
        </Form.Item>
        <Form.Item
          validateStatus={getErrorField('upload_file') ? 'error' : ''}
          help={getErrorField('upload_file') || ''}
          label="Upload File Voucher Code"
          extra="maximun size 500kb"
          hasFeedback
          style={{ marginBottom: 8 }}
          {...formItemLayout}
        >
          {form.getFieldDecorator('upload_file', {
            rules: [{ required: true, message: 'Field required!' }]
          })(<Input
            type="file"
            size="large"
            name="upload_file"
            id="upload_file"
            placeholder="file URL"
            onChange={this.handleExelUpload('fileExcel')}
          />)}
        </Form.Item>
        <div style={{ marginLeft: '25%', marginBottom: '12px' }}>
          <a
            style={{ width: '40%' }}
            download="template_code"
            target="_blank"
            href="/assets/files/Template_Upload_Voucher_Code.xlsx">
            Download file template here
          </a>
        </div>   
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('buy_voucher_available') ? 'error' : ''}
          help={getErrorField('buy_voucher_available') || ''}
          label="Voucher Available"
          {...formItemLayout}
        >
          {form.getFieldDecorator('buy_voucher_available', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <InputNumber
              style={{ width: '30%' }}
              min={0}
              name="buy_voucher_available"
              placeholder="Voucher Available"
              type="number"
            />
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('voucher_price') ? 'error' : ''}
          help={getErrorField('voucher_price') || ''}
          label="Voucher Price"
          {...formItemLayout}
        >
          {form.getFieldDecorator('voucher_price', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <InputNumber
              style={{ width: '30%' }}
              min={0}
              name="voucher_price"
              placeholder="Voucher Price"
              type="number"
            />
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('revenue_sharing_boost') ? 'error' : ''}
          help={getErrorField('revenue_sharing_boost') || ''}
          label="Revenue Sharing Boost"
          {...formItemLayout}
        >
          {form.getFieldDecorator('revenue_sharing_boost', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <InputNumber
              style={{ width: '30%' }}
              min={0}
              name="revenue_sharing_boost"
              placeholder="Revenue Sharing Boost"
              type="number"
            />
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('revenue_sharing_merchant') ? 'error' : ''}
          help={getErrorField('revenue_sharing_merchant') || ''}
          label="Revenue"
          {...formItemLayout}
        >
          {form.getFieldDecorator('revenue_sharing_merchant', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <InputNumber
              style={{ width: '30%' }}
              min={0}
              name="revenue_sharing_merchant"
              placeholder="Revenue"
              type="number"
            />
          )}
        </Form.Item>
      </div>
    );
  }
}

export default PromoBuyVoucher;
