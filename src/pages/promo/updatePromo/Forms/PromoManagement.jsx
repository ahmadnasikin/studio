import React from 'react';
import PropTypes from 'prop-types';
import {
  Divider,
  Form,
  Select,
  Input,
  Radio,
  DatePicker,
  message
} from 'antd';
import axios from 'axios';

import { typeList } from '../../../../utils/typeList';
import { formItemLayout } from '../../../../styles/component/formVariable';
import config from '../../../../../config';

class PromoManagement extends React.Component {
  static propTypes = {
    form: PropTypes.instanceOf(Object)
  }

  state = {
    categories: {},
    primaryImage: null,
    headerDetailImage: null,
    hotPromoImage: null
  }

  getImageUrl = (text) => {
    return text === null ? 'No photo available' : text;
  }

  handleFileUpload = name => (e) => {
    const file_ = e.target.files[0];
    const fileSize = parseInt(file_.size, 10);
    if (fileSize > 1000000) {
      Notification(
        'error',
        'Maximum file size is 1Mb!',
      );
      return;
    }
    const formData = new FormData();
    formData.append('image', file_);
    formData.append('imageName', 'promozone');
    const Auth = config.Authorization;
    const options = {
      headers: {
        'content-type': 'multipart/form-data',
        Authorization: Auth
      }
    };
    axios.post(`${config.apiUrl}utils/upload`, formData, options)
    .then(({ data }) => {
      const urlImage = `${data.baseUrl}/${data.imageName}`;
      this.setState({
        [name]: urlImage
      });
      message.success(`${data.imageName} file uploaded successfully`);
    })
    .catch((err) => {
      Notification(
        'error',
        'Oops! Can\'t upload photo',
      );
    });
  }

  render() {
    const {
      form,
      getErrorField
    } = this.props;
    const {
      categories,
      primaryImage,
      headerDetailImage,
      hotPromoImage
    } = this.state;


    return (
      <div>
        <Divider>Promo Management</Divider>
        <Form>

          {/* Promo Type Field */}
          <Form.Item
            className="v-center"
            validateStatus={getErrorField('promo_type_id') ? 'error' : ''}
            help={getErrorField('promo_type_id') || ''}
            label="Promo Type"
            {...formItemLayout}
          >
            {form.getFieldDecorator('promo_type_id', {
              rules: [{ required: true, message: 'Field required!' }],
              initialValue: 1
            })(
              <Select
                showSearch
                style={{ width: '50%' }}
                name="promoType"
                placeholder="Select Promo Type"
                optionFilterProp="children"
                // filterOption={this.handleFilterOption}
                // onChange={this.typeChange('promoType')}
              >
                {Object.keys(typeList).map((value, index) => {
                  return (
                    <Select.Option key={index} value={typeList[value].index}>{value}</Select.Option>
                  );
                })}
              </Select>
            )}
          </Form.Item>

          {/* Primary Image Field */}
          <Form.Item
            className="v-center"
            validateStatus={getErrorField('primaryImage') ? 'error' : ''}
            help={getErrorField('primaryImage') || ''}
            label="Primary Image"
            hasFeedback
            extra={this.getImageUrl(primaryImage)}
            {...formItemLayout}
          >
            {form.getFieldDecorator('primaryImage', {
              rules: [{ required: false, message: 'Field required!' }]
            })(<Input
              type="file"
              size="large"
              name="primaryImage"
              id="primaryImage"
              placeholder="Image URL"
              onChange={this.handleFileUpload('primaryImage')}
            />)}
          </Form.Item>

          {/* Header Detaim Image Field */}
          <Form.Item
            className="v-center"
            validateStatus={getErrorField('headerDetailImage') ? 'error' : ''}
            help={getErrorField('headerDetailImage') || ''}
            label="Header Detail Image"
            hasFeedback
            extra={this.getImageUrl(headerDetailImage)}
            {...formItemLayout}
          >
            {form.getFieldDecorator('headerDetailImage', {
              rules: [{ required: false, message: 'Field required!' }]
            })(<Input
              type="file"
              size="large"
              name="headerDetailImage"
              id="headerDetailImage"
              placeholder="Image URL"
              onChange={this.handleFileUpload('headerDetailImage')}
            />)}
          </Form.Item>

          {/* Status Field */}
          <Form.Item
            className="v-center"
            validateStatus={getErrorField('status') ? 'error' : ''}
            help={getErrorField('status') || ''}
            label="Status"
            {...formItemLayout}
          >
            {form.getFieldDecorator('status', {
              rules: [{ required: true, message: 'Field required!' }],
              defaultValue:  0
            })(
              <Radio.Group>
                <Radio.Button value="0">Draft</Radio.Button>
                <Radio.Button value="1">Publish</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>

          {/* Categories Field */}
          <Form.Item
            className="v-center"
            validateStatus={getErrorField('category_id') ? 'error' : ''}
            help={getErrorField('category_id') || ''}
            label="Categories"
            {...formItemLayout}
          >
            {form.getFieldDecorator('category_id', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Select
                showSearch
                style={{ width: '50%' }}
                name="promoType"
                placeholder="Select Categories"
                optionFilterProp="children"
                // filterOption={this.handleFilterOption}
              >
                {Object.keys(categories).map((value, index) => {
                  return (
                    <Select.Option key={index} value={value}>{categories[value]}</Select.Option>
                  );
                })}
              </Select>
            )}
          </Form.Item>

          {/* Date Campaign Field */}
          <Form.Item
            className="v-center"
            validateStatus={getErrorField('date_campaign_start') ? 'error' : ''}
            help={getErrorField('date_campaign_start') || ''}
            label="Date Campaign"
            {...formItemLayout}
          >
            {form.getFieldDecorator('date_campaign', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <DatePicker.RangePicker />
            )}
          </Form.Item>

          {/* Hot in Hot Promo Field */}
          <Form.Item
            className="v-center"
            validateStatus={getErrorField('show_hot_promo') ? 'error' : ''}
            help={getErrorField('show_hot_promo') || ''}
            label="Hot in Hot Promo"
            {...formItemLayout}
          >
            {form.getFieldDecorator('show_hot_promo', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Radio.Group>
                <Radio.Button value="0">No</Radio.Button>
                <Radio.Button value="1">Yes</Radio.Button>
              </Radio.Group>
            )}
          </Form.Item>

          {/* Hot Promo Image Field */}
          <Form.Item
            className="v-center"
            validateStatus={getErrorField('hotPromoImage') ? 'error' : ''}
            help={getErrorField('hotPromoImage') || ''}
            label="Hot Promo Image"
            hasFeedback
            extra={this.getImageUrl(hotPromoImage)}
            {...formItemLayout}
          >
            {form.getFieldDecorator('hot_promo_image', {
              rules: [{ required: false, message: 'Field required!' }]
            })(<Input
              type="file"
              size="large"
              name="hotPromoImage"
              id="headerDetailImage"
              placeholder="Image URL"
              onChange={this.handleFileUpload('hotPromoImage')}
            />)}
          </Form.Item>

          {/* Hot Promo Position Field */}
          <Form.Item
            className="v-center"
            validateStatus={getErrorField('hot_promo_position') ? 'error' : ''}
            help={getErrorField('hot_promo_position') || ''}
            label="Hot Promo Position"
            {...formItemLayout}
          >
            {form.getFieldDecorator('hot_promo_position', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Radio.Group>
                <Radio value="1">1</Radio>
                <Radio value="2">2</Radio>
                <Radio value="3">3</Radio>
                <Radio value="4">4</Radio>
                <Radio value="5">5</Radio>
                <Radio value="6">6</Radio>
                <Radio value="7">7</Radio>
                <Radio value="8">8</Radio>
                <Radio value="9">9</Radio>
                <Radio value="10">10</Radio>
              </Radio.Group>
            )}
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default PromoManagement;
