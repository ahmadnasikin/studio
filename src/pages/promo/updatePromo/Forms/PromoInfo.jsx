import React from 'react';
import PropTypes from 'prop-types';
import { Form, Divider, Input } from 'antd';

import { formItemLayout } from '../../../../styles/component/formVariable';

const PromoInfo = (props) => {
  const {
    form,
    getErrorField
  } = props;


  return (
    <div>
      <Form>
        <Divider>Promo Info</Divider>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('promo_name') ? 'error' : ''}
          help={getErrorField('promo_name') || ''}
          label="Promo Name"
          {...formItemLayout}
        >
          {form.getFieldDecorator('promo_name', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <Input
              name="promo_name"
              placeholder="Enter Promo Name"
            />
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('promo_validation') ? 'error' : ''}
          help={getErrorField('promo_validation') || ''}
          label="Promo Validation"
          {...formItemLayout}
        >
          {form.getFieldDecorator('promo_validation', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <Input
              name="promo_validation"
              placeholder="Promo Validation"
            />
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('detail') ? 'error' : ''}
          help={getErrorField('detail') || ''}
          label="Detail"
          {...formItemLayout}
        >
          {form.getFieldDecorator('detail', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <Input.TextArea
              name="detail"
              placeholder="Enter the Detail"
            />
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('how_to_use') ? 'error' : ''}
          help={getErrorField('how_to_use') || ''}
          label="How To Use"
          {...formItemLayout}
        >
          {form.getFieldDecorator('how_to_use', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <Input.TextArea
              name="how_to_use"
              placeholder="How to Use"
            />
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('tnc') ? 'error' : ''}
          help={getErrorField('tnc') || ''}
          label="Term & Condition"
          {...formItemLayout}
        >
          {form.getFieldDecorator('tnc', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <Input.TextArea
              name="tnc"
              placeholder="Term & Condition"
            />
          )}
        </Form.Item>
      </Form>
    </div>
  );
};

PromoInfo.propTypes = {
  form: PropTypes.instanceOf(Object),
  getErrorField: PropTypes.func
};

export default PromoInfo;
