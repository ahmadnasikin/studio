import React from 'react';
import {
  Divider,
  Form,
  Select,
  Input,
  InputNumber
} from 'antd';

import { formItemLayout } from '../../../../styles/component/formVariable';
import { days } from '../../../../utils/dayList';

class PromoGetVoucher extends React.PureComponent {
  render() {
    const {
      form,
      getErrorField
    } = this.props;
    return (
      <div>
        <Divider>Promo Type: Get Voucher(Promo Code)</Divider>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('target_user') ? 'error' : ''}
          help={getErrorField('target_user') || ''}
          label="Target User"
          {...formItemLayout}
        >
          {form.getFieldDecorator('target_user', {
            rules: [{ required: true, message: 'Field required!' }],
            initialValue: 'All User'
          })(
            <Input
              disabled
              style={{ width: '40%' }}
              min={0}
              name="revenue_sharing_merchant"
              placeholder="All User"
            />
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('reward_type_id') ? 'error' : ''}
          help={getErrorField('reward_type_id') || ''}
          label="Reward Type"
          {...formItemLayout}
        >
          {form.getFieldDecorator('reward_type_id', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <Select
              style={{ width: '40%' }}
              showSearch
              placeholder="Select Type Reward"
              optionFilterProp="children"
              name="promoTypeReward"
              // onChange={this.typeChange('promoTypeReward')}
            >
              <Select.Option value="1">CashBack Rupiah</Select.Option>
              <Select.Option value="2">Coupon</Select.Option>
              <Select.Option value="3">Persentage</Select.Option>
            </Select>
          )}
        </Form.Item>
        {/* { promoTypeReward === '1' && inputCashBackNominal }
        { promoTypeReward === '2' && inputCashBackCoupon }
        { promoTypeReward === '3' && inputCashBackPresentage } */}
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('day_active') ? 'error' : ''}
          help={getErrorField('day_active') || ''}
          label="Day Active"
          {...formItemLayout}
        >
          {form.getFieldDecorator('day_active', {
            rules: [
              { required: true, message: 'Field is required', type: 'array' }
            ],
            defaultValue: []
          })(
            <Select
              style={{ width: '80%'}}
              mode="multiple"
              placeholder="Please select day"
            >
              {Object.keys(days).map((value, index) => {
                return (
                  <Select.Option key={index} value={days[value].index}>{value}</Select.Option>
                );
              })}
            </Select>
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('time Active') ? 'error' : ''}
          help={getErrorField('time Active') || ''}
          label="Time Active"
          {...formItemLayout}
        >
          {form.getFieldDecorator('time_active', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <Select
              style={{ width: '40%' }}
              showSearch
              placeholder="Select Time"
              optionFilterProp="children"
              name="timeActive"
              // onChange={this.typeChange('timeActive')}
            >
              <Select.Option value="1">All Time</Select.Option>
              <Select.Option value="2">Range Time</Select.Option>
            </Select>
          )}
        </Form.Item>
        {/* { timeActive === '2' && inputTimeActive } */}
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('max_reward_user') ? 'error' : ''}
          help={getErrorField('max_reward_user') || ''}
          label="Max Reward User"
          {...formItemLayout}
        >
          {form.getFieldDecorator('max_reward_user', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <InputNumber
              style={{ width: '40%' }}
              min={0}
              name="max_reward_user"
              placeholder="Please Input Max User"
              type="number"
            />
          )}
        </Form.Item>
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('Max User Get Reward') ? 'error' : ''}
          help={getErrorField('Max User Get Reward') || ''}
          label="Max User Get Reward"
          {...formItemLayout}
        >
          <Select
            style={{ width: '40%' }}
            showSearch
            placeholder="Please Select Type"
            optionFilterProp="children"
            name="maxUserGetReward"
            // onChange={this.typeChange('maxUserGetReward')}
          >
            <Select.Option value="1">User Per Day</Select.Option>
            <Select.Option value="2">User On Date</Select.Option>
            <Select.Option value="3">No Limit Reward</Select.Option>
          </Select>
        </Form.Item>
        {/* { maxUserGetReward === '1' && inputUserPerDay }
        { maxUserGetReward === '2' && inputUserOnDate } */}
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('Merchant Specific') ? 'error' : ''}
          help={getErrorField('Merchant Specific') || ''}
          label="Merchant Specific"
          {...formItemLayout}
        >
          <Select
            style={{ width: '40%' }}
            showSearch
            placeholder="Please Select Type"
            optionFilterProp="children"
            name="merchantSpecific"
            // onChange={this.typeChange('merchantSpecific')}
          >
            <Select.Option value="1">Multiple Merchants</Select.Option>
            <Select.Option value="2">All Merchant Boost</Select.Option>
          </Select>
        </Form.Item>
        {/* { merchantSpecific === '1' && inputMerchantSpecific } */}
        <Form.Item
          className="v-center"
          validateStatus={getErrorField('get_voucher_available') ? 'error' : ''}
          help={getErrorField('get_voucher_available') || ''}
          label="Voucher Available"
          {...formItemLayout}
        >
          {form.getFieldDecorator('get_voucher_available', {
            rules: [{ required: true, message: 'Field required!' }]
          })(
            <InputNumber
              style={{ width: '30%' }}
              min={0}
              name="get_voucher_available"
              placeholder="Voucher Available"
              type="number"
            />
          )}
          <span className="ant-form-text">pcs</span>
        </Form.Item>
      </div>
    );
  }
}

export default PromoGetVoucher;
