import React from 'react';
import PropTypes from 'prop-types';
import { Modal, Button } from 'antd';

import { typeList } from '../../../utils/typeList';

const styles = {
  root: {
    display: 'flex',
    width: '100%',
    flexWrap: 'wrap'
  },
  wrapper: {
    width: '100%',
    display: 'flex'
  },
  label: {
    width: '20%'
  },
  detail: {
    width: '80%'
  }
};

const Preview = (props) => {
  const {
    data,
    show,
    loading,
    onCancel,
    onSubmit
  } = props;

  const getFieldVisible = (type) => {
    switch (type) {
      case 'PROMO_TYPE':
        return true;
      case 'VOUCHER_AVAILABLE':
        if (data.promo_type_id === 2) {
          return true;
        }
        return false;
      default:
        return false;
    }
  };

  const getMaskedField = (type, value) => {
    let maskedValue = value;
    switch (type) {
      case 'PROMO_TYPE':
        Object.keys(typeList).some((key) => {
          if (typeList[key].index === value) {
            maskedValue = key;
            return true;
          }
          return false;
        });
        return maskedValue;
      default:
        return value;
    }
  };

  return (
    <Modal
      title="Preview Form Before Submit"
      visible={show}
      onCancel={onCancel}
      width="85%"
      footer={[
        <Button key="back" onClick={onCancel}>Cancel</Button>,
        <Button key="submit" type="primary" loading={loading} onClick={onSubmit}>
          Submit
        </Button>
      ]}
    >
      <div style={styles.root}>
        {getFieldVisible('PROMO_TYPE') &&
          <div style={styles.wrapper}>
            <div style={styles.label}>Promo</div>
            <div style={styles.detail}>{getMaskedField('PROMO_TYPE', data.promo_type_id)}</div>
          </div>
        }
        {getFieldVisible('VOUCHER_AVAILABLE') &&
          <div style={styles.wrapper}>
            <div style={styles.label}>Promo</div>
            <div style={styles.detail}>{data.buy_voucher_available}</div>
          </div>
        }
      </div>
    </Modal>
  );
};

Preview.propTypes = {
  data: PropTypes.instanceOf(Object),
  show: PropTypes.bool,
  loading: PropTypes.bool,
  onCancel: PropTypes.func,
  onSubmit: PropTypes.func
};

export default Preview;
