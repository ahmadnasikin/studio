import React from 'react';
import {
  Spin,
  Form,
  Input,
  Radio,
  Select,
  Button,
  Switch,
  // Icon,
  message,
  DatePicker
} from 'antd';
import axios from 'axios';
import moment from 'moment';
import customAxios from '../../../utils/axios';
import { formItemLayout, btn } from '../../../styles/component/formVariable';
import config from '../../../../config';
import Notification from '../../../components/notifications/notifications';
import { gameList } from '../../../utils/gamesList';

const FormItem = Form.Item;
const { Option } = Select;
// let uuid = 1;
let errSubmit = false;
class DichotomiCreate extends React.Component {
  state = {
    loadingPage: true,
    loading: false,
    image: null,
    // isUploading: false,
    // uploading: false,
    endValue: null
    // endOpen: false
  }

  componentDidMount() {
    // To disabled submit button at the beginning.
    const { form: { validateFields } } = this.props;
    validateFields();
    this.setLoading(false);
  }

  setLoading(status) {
    setTimeout(() => {
      this.setState({ loadingPage: status });
    }, 1000);
  }

  disabledStartDate = (startValue) => {
    const { endValue } = this.state;
    if (!startValue || !endValue) {
      return false;
    }
    return startValue.valueOf() > endValue.valueOf();
  }

  onChange = (field, value) => {
    this.setState({
      [field]: value
    });
  }

  onEndChange = (value) => {
    this.onChange('endValue', value);
  }

  handleReset = () => {
    const { form: { resetFields } } = this.props;
    resetFields();
  }

  // handleStartOpenChange = (open) => {
  //   if (!open) {
  //     this.setState({ endOpen: true });
  //   }
  // }

  // handleEndOpenChange = (open) => {
  //   this.setState({ endOpen: open });
  // }

  // hanldeMultiple = () => {
  //   const children = [];
  //   for (let i = 10; i < 36; i++) {
  //     children.push(<Option key={i.toString(36) + i}>{i.toString(36) + i}</Option>);
  //   }
  // }

  handleFileUpload = (e) => {
    const file_ = e.target.files[0];
    const fileSize = parseInt(file_.size, 10);
    if (fileSize > 1000000) {
      Notification(
        'error',
        'Maximum file size is 1Mb!',
      );
      return;
    }
    const formData = new FormData();
    formData.append('file', file_);
    formData.append('upload_preset', config.cloudinaryUploadPreset);
    formData.append('api_key', config.cloudinaryApiKey);
    formData.append('api_secret', config.cloudinaryApiSecret);
    formData.append('cloud_name', config.cloudinaryCloudName);
    const options = {
      headers: {
        'content-type': 'multipart/form-data'
      }
    };
    axios.post(config.cloudinaryUploadUrl, formData, options)
    .then(({ data }) => {
      const secureUrl_ = ({}).hasOwnProperty.call(data, 'secure_url') ? data.secure_url : null;
      this.setState({
        image: secureUrl_
      });
      message.success(`${data.original_filename} file uploaded successfully`);
    })
    .catch((err) => {
      Notification(
        'error',
        'Oops! Can\'t upload photo to cloudinary',
      );
    });
  }

  handleSubmit = (e) => {
    e.preventDefault();
    this.setState({
      loading: true
    });
    const { form: { validateFields } } = this.props;
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const { image } = this.state;
    validateFields((err, values) => {
      if (!err) {
        // const reward = values.reward_type.map((data, i) => {
        //   const key = i + 1;
        //   const intToString = key.toString();
        //   return {
        //     position: intToString,
        //     type: data,
        //     value: values.reward_value[i]
        //   };
        // });
        const groupName = values.group_name.map((data) => {
          return {
            name: data
          };
        });
        const imageState = image === null ? 'no-image' : image;
        const isSticky = values.is_sticky ? 1 : 0;
        const date = moment(values.ended_at, 'YYYY-MM-DD');
        const payload = {
          game_id: values.game_id,
          name: values.name,
          description: values.description,
          status: values.status,
          ended_at: date,
          group_name: groupName,
          is_sticky: isSticky,
          image: imageState,
          reward: []
        };
        customAxios({
          method: 'post',
          url: `${config.apiUrl}content/dichotomy`,
          data: payload,
          headers: { Authorization: token }
        }).then(() => {
          this.setState({ loading: false });
          this.handleReset();
          Notification('success', 'Success create dichotomy');
        }).catch(() => {
          this.setState({ loading: false });
          Notification('error', 'Oops! Can\'t create dichotomy');
        });
      } else {
        errSubmit = true;
        this.setState({ loading: false });
        Notification('error', 'Please complete the fields');
      }
    });
  }

  hasErrors = (fieldsError) => {
    return Object.keys(fieldsError).some(field => fieldsError[field]);
  }

  getErrorField = (fieldName) => {
    const { form: { isFieldTouched, getFieldError } } = this.props;
    let fieldTouched = isFieldTouched(fieldName);
    if (errSubmit) {
      fieldTouched = true;
    }
    const fieldErr = getFieldError(fieldName);
    return fieldTouched && fieldErr;
  }

  // sometime to use add reward
  // addRewardItems = () => {
  //   const { form } = this.props;
  //   const keys = form./* getFieldValue */('keys');
  //   const nextKeys = keys.concat(uuid);
  //   uuid += 1;
  //   form.setFieldsValue({
  //     keys: nextKeys
  //   });
  // }

  // removeRewardItems = (k) => {
  //   const { form } = this.props;
  //   // can use data-binding to get
  //   const keys = form./* getFieldValue */('keys');
  //   // We need at least one passenger
  //   if (keys.length === 1) {
  //     return;
  //   }

  //   // can use data-binding to set
  //   form.setFieldsValue({
  //     keys: keys.filter(key => key !== k)
  //   });
  // }

  handleFilterOption = (input, option) => option.props.children.toLowerCase().indexOf(input.toLowerCase()) >= 0;

  normFile = (e) => {
    if (Array.isArray(e)) {
      return e;
    }
    return e && e.fileList;
  }

  render() {
    const { form: { getFieldDecorator /* getFieldValue */ } } = this.props;
    const {
      loadingPage,
      loading,
      image
    } = this.state;
    const optionGameLists = Object.keys(gameList).map((value, index) => {
      return (
        <Option key={index} value={gameList[value].index}>{value}</Option>
      );
    });

    const loadingView = (
      <div><center><Spin /></center></div>
    );

    // add reward
    // getFieldDecorator('keys', { initialValue: [0] });
    // const keys = /* getFieldValue */('keys');
    // const rewardItems = keys.map((k, index) => {
    //   return (
    //     <div key={index}>
    //       <FormItem
    //         key={`${index}-1`}
    //         className="v-center"
    //         style={{ marginBottom: 10 }}
    //         validateStatus={this.getErrorField(`reward_type[${k}]`) ? 'error' : ''}
    //         help={this.getErrorField(`reward_type[${k}]`) || ''}
    //         label={`Reward ${index + 1}`}
    //         {...formItemLayout}
    //       >
    //         {getFieldDecorator(`reward_type[${k}]`, {
    //           rules: [{ required: true, message: 'Field required!' }],
    //           initialValue: 'COUPON'
    //         })(
    //           <Select
    //             style={{ width: '43%' }}
    //             placeholder="Enter the Reward"
    //           >
    //             <Option value="COUPON">COUPON</Option>
    //           </Select>
    //         )}
    //         {keys.length > 1
    //         ? <div
    //           style={{ width: '10%', paddingLeft: 6, display: 'inline-block' }}
    //         >
    //           <Icon
    //             style={{ cursor: 'pointer' }}
    //             className="dynamic-delete-button"
    //             type="minus-circle-o"
    //             disabled={keys.length === 1}
    //             onClick={() => this.removeRewardItems(k)}
    //           />
    //         </div>
    //         : ''
    //         }
    //       </FormItem>
    //       <FormItem
    //         key={`${index}-2`}
    //         validateStatus={this.getErrorField(`reward_value[${k}]`) ? 'error' : ''}
    //         help={this.getErrorField(`reward_value[${k}]`) || ''}
    //         {...btn}
    //       >
    //         {getFieldDecorator(`reward_value[${k}]`, {
    //           rules: [{ required: true, message: 'Field required!' }],
    //           initialValue: ''
    //         })(
    //           <Input
    //             placeholder="Enter the Reward"
    //           />
    //         )}
    //       </FormItem>
    //     </div>
    //   );
    // });

    const view = (
      <div>
        <div>
          <h3 className="isoBoxTitle">Create Dichotomy</h3>
        </div>
        <Form onSubmit={this.handleSubmit}>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('image') ? 'error' : ''}
            help={this.getErrorField('image') || ''}
            label="Image"
            hasFeedback
            extra={image === null ? 'No photo available' : image}
            {...formItemLayout}
          >
            {getFieldDecorator('image', {
              rules: [{ required: false, message: 'Field required!' }]
            })(<Input
              type="file"
              size="large"
              name="image"
              id="image"
              placeholder="Image URL"
              onChange={this.handleFileUpload}
            />)}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('game_id') ? 'error' : ''}
            help={this.getErrorField('game_id') || ''}
            label="Game Id"
            {...formItemLayout}
          >
            {getFieldDecorator('game_id', {
              rules: [{ required: false, message: 'Field required!' }],
              initialValue: null
            })(
              <Select
                showSearch
                style={{ width: '50%' }}
                name="game_id"
                placeholder="Select the game"
                optionFilterProp="children"
                filterOption={this.handleFilterOption}
              >
                {optionGameLists}
              </Select>
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('name') ? 'error' : ''}
            help={this.getErrorField('name') || ''}
            label="Name"
            {...formItemLayout}
          >
            {getFieldDecorator('name', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Input
                name="name"
                placeholder="Enter the name"
              />
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('description') ? 'error' : ''}
            help={this.getErrorField('description') || ''}
            label="Description"
            {...formItemLayout}
          >
            {getFieldDecorator('description', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Input.TextArea
                name="description"
                placeholder="Enter the description"
              />
            )}
          </FormItem>
          <FormItem
            className="v-center"
            style={{ marginBottom: 10 }}
            validateStatus={this.getErrorField('group_name[0]') ? 'error' : ''}
            help={this.getErrorField('group_name[0]') || ''}
            label="Group Name"
            {...formItemLayout}
          >
            {getFieldDecorator('group_name[0]', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Input
                name="group_name[0]"
                placeholder="Enter the Group Name 1"
              />
            )}
          </FormItem>
          <FormItem
            className="v-center"
            style={{ marginBottom: 10 }}
            validateStatus={this.getErrorField('group_name[1]') ? 'error' : ''}
            help={this.getErrorField('group_name[1]') || ''}
            label="Group Name"
            {...formItemLayout}
          >
            {getFieldDecorator('group_name[1]', {
              rules: [{ required: true, message: 'Field required!' }]
            })(
              <Input
                name="group_name[0]"
                placeholder="Enter the Group Name 2"
              />
            )}
          </FormItem>
          {/* column add reward */}
          {/* {rewardItems}

          <FormItem {...btn}>
            <Button
              type="primary"
              block
              ghost
              onClick={this.addRewardItems}
            >
              + Add Reward
            </Button>
          </FormItem> */}
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('status') ? 'error' : ''}
            help={this.getErrorField('status') || ''}
            label="Status"
            {...formItemLayout}
          >
            {getFieldDecorator('status', {
              rules: [{ required: true, message: 'Field required!' }],
              initialValue: 0
            })(
              <Radio.Group>
                <Radio.Button value="0">Non Active</Radio.Button>
                <Radio.Button value="1">Active</Radio.Button>
              </Radio.Group>
            )}
          </FormItem>
          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('is_sticky') ? 'error' : ''}
            help={this.getErrorField('is_sticky') || ''}
            label="Is Sticky"
            {...formItemLayout}
          >
            {getFieldDecorator('is_sticky', {
              rules: [{ required: true, message: 'Field required!' }],
              initialValue: 0
            })(
              <Switch />
            )}
          </FormItem>

          <FormItem
            className="v-center"
            validateStatus={this.getErrorField('ended_at') ? 'error' : ''}
            help={this.getErrorField('ended_at') || ''}
            label="End Date"
            {...formItemLayout}
          >
            {getFieldDecorator('ended_at', {
              rules: [{ required: true, message: 'Field required!' }]
            })(<DatePicker />)}
          </FormItem>

          <FormItem {...btn}>
            <Button
              type="primary"
              htmlType="submit"
              loading={loading}
              className="update-form-button"
            >
              Submit
            </Button>
          </FormItem>
        </Form>
      </div>
    );
    return (
      <div>
        { loadingPage ? loadingView : view }
      </div>
    );
  }
}

const WrappedDichotomyCreate = Form.create()(DichotomiCreate);

export default WrappedDichotomyCreate;
