import React from 'react';
import {
  Input, Button, Table, Icon, Divider, Modal, message, Form, DatePicker
} from 'antd';
import moment from 'moment';
import momentTZ from 'moment-timezone';
import { Link } from 'react-router-dom';
import axios from '../../../utils/axios';
import config from '../../../../config';

const { Search } = Input;
const { confirm } = Modal;
let idPublishFeed = null;

class ListDichotomy extends React.Component {
  state = {
    data: [],
    pagination: {
      total: 0,
      current: 1,
      pages: 1,
      limit: 10
    },
    sort: null,
    order: 'ASC',
    loading: false,
    searchKey: '',
    visibleModalFeed: false,
    loadingModalFeed: false,
    publishDate: moment()
  }

  componentDidMount() {
    this.fetch();
  }

  handleTableChange = (pagination, filter, sorter) => {
    setTimeout(() => {
      const { pagination: pageState } = this.state;
      const pager = {
        ...pageState,
        current: pagination.current
      };
      const order = sorter.order === 'ascend' ? 'ASC' : 'DESC';
      this.setState({
        pagination: pager,
        sort: sorter.field,
        order
      },
      () => {
        this.fetch();
      });
    }, 400);
  }

  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.fetch();
    }
  }

  handleSearch = (e) => {
    e.preventDefault();
    this.setState({ searchKey: e.target.value });
  }

  fetch = () => {
    const {
      pagination: { current, limit }, searchKey, sort, order
    } = this.state;
    const searchTerm = searchKey !== '' ? searchKey : null;
    const params = {
      page: current,
      limit,
      search_term: searchTerm,
      sort,
      order
    };
    this.setState({ loading: true });
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}content/dichotomy`;
    axios({
      method: 'get',
      url: apiPath,
      params: {
        ...params
      },
      headers: { Authorization: token }
    }).then((response) => {
      const { pagination } = this.state;
      const { data: { result: { data } } } = response;
      pagination.total = data.total;
      pagination.pages = data.pages;
      pagination.limit = data.limit;
      const dichotomy = data.docs.map((dicho, index) => {
        const createdAt = momentTZ(dicho.created_at).tz('Asia/Jakarta').format('YYYY-MM-DD');
        const endedAt = momentTZ(dicho.ended_at).tz('Asia/Jakarta').format('YYYY-MM-DD');
        const status = dicho.status === 1 ? 'Active' : 'Non Active';
        const isFeed = dicho.is_feeds === 1;
        return {
          countReward: dicho.reward.length,
          key: dicho._id,
          name: dicho.name,
          created_at: createdAt,
          ended_at: endedAt,
          index: (index + 1) + ((pagination.current - 1) * pagination.limit),
          status,
          isFeed
        };
      });
      this.setState({
        loading: false,
        data: dichotomy,
        pagination
      });
    }).catch((err) => {
      console.log(err); /* eslint-disable-line no-console */
    });
  }

  deleteDichotomy = (id) => {
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}content/dichotomy/${id}/delete`;
    axios({
      method: 'post',
      url: apiPath,
      headers: { Authorization: token }
    }).then(({ data }) => {
      if (data.status === 200 && data.success) {
        this.success();
        this.fetch();
      } else if (data.error && (data.error.status === 400)) {
        message.warning(data.error.message);
      }
    }).catch((err) => {
      this.error();
      console.log(err); /* eslint-disable-line no-console */
    });
  }

  showDeleteConfirm = (id) => {
    const deleteDechotomy = this.deleteDichotomy;
    confirm({
      title: 'Are you sure delete this dichotomy?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        deleteDechotomy(id);
      },
      onCancel: () => {}
    });
  }

  showModalFeed = (id) => {
    idPublishFeed = id;
    this.setState({
      visibleModalFeed: true
    });
  }

  cancelExportFeed = () => {
    this.setState({
      visibleModalFeed: false
    });
  }

  exportToFeed = () => {
    const {
      state, getDechotilById, insertToFeed
    } = this;
    let { publishDate } = state;
    publishDate = moment(publishDate, 'YYYY-MM-DD');
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { username } = user;
    this.setState({
      loadingModalFeed: true
    });
    getDechotilById(idPublishFeed)
    .then(({ data: { result } }) => {
      const dichotomy = result;

      const dataFeed = {
        type: 'DICHOTOMY',
        created_by: username,
        publish_at: publishDate,
        dichotomy
      };

      insertToFeed(dataFeed).then((response) => {
        this.setState({
          loadingModalFeed: false,
          visibleModalFeed: false
        });
        this.fetch();
      });
    });
  }

  insertToFeed = (data) => {
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const url = `${config.apiUrl}content/feed`;
    return axios({
      method: 'post',
      url,
      data,
      headers: { Authorization: token }
    });
  }

  getDechotilById = (id) => {
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}content/dichotomy/${id}`;
    return axios({
      method: 'get',
      url: apiPath,
      headers: { Authorization: token }
    });
  }

  handleChangeDate = (e) => {
    this.setState({
      publishDate: e
    });
  }

  success = () => {
    message.success('success delete dichotomy');
  };

  error = () => {
    message.error('internal server error');
  };

  render() {
    const delStyle = {
      cursor: 'pointer',
      color: 'red'
    };
    const exportStyle = {
      cursor: 'pointer',
      color: 'green'
    };
    const {
      data, pagination, loading, searchKey, visibleModalFeed, loadingModalFeed, publishDate
    } = this.state;
    const columns = [{
      title: '#',
      dataIndex: 'index'
    }, {
      title: 'Name',
      dataIndex: 'name',
      sorter: true,
      width: '300px'
    }, {
      title: 'Status',
      dataIndex: 'status',
      sorter: true
    }, {
      title: 'Create At',
      dataIndex: 'created_at',
      sorter: true
    }, {
      title: 'Ended At',
      dataIndex: 'ended_at',
      sorter: true
    }, {
      title: 'Action',
      dataIndex: 'action',
      render: (text, record) => (
        <span>
          <Link
            to={
            {
              pathname: `content/dichotomy/detail/${record.key}/`
            }
          }
            title="Detail"
          >
            <Icon type="eye" />
            <Divider type="vertical" />
          </Link>
          <Link
            to={
            {
              pathname: `/content/dichotomy/edit/${record.key}/`
            }
          }
            title="Edit"
          >
            <Icon type="edit" />
            <Divider type="vertical" />
          </Link>
          { !record.isFeed &&
            <span
              style={exportStyle}
              title="Add to Feed"
              role="presentation"
              tabIndex="-1"
              onClick={() => this.showModalFeed(record.key)}
            >
              <Icon type="export" />
              <Divider type="vertical" />
            </span>
          }
          <span
            style={delStyle}
            title="Delete"
            role="presentation"
            tabIndex="-1"
            onClick={() => this.showDeleteConfirm(record.key)}
          >
            <Icon type="delete" />
          </span>
        </span>
      )
    }];
    return (
      <div>
        <h1>List Dichotomy</h1>
        <div className="btn__wrapper">
          <div className="btn__wrapper--left">
            <Search
              placeholder="input search text"
              className="search-wrapper"
              size="small"
              style={{ height: 30 }}
              onChange={this.handleSearch}
              onKeyPress={this._handleKeyPress}
              value={searchKey}
            />
          </div>
          <div className="btn__wrapper--right">
            <Link to="/create-dichotomy">
              <Button type="ghost" htmlType="submit" className="btn-success" icon="plus-circle">Add New</Button>
            </Link>
          </div>
        </div>
        <div className="table-wrapper">
          <Table
            className="table"
            columns={columns}
            dataSource={data}
            pagination={pagination}
            loading={loading}
            onChange={this.handleTableChange}
          />
        </div>
        <Modal
          title="Add to Feed"
          visible={visibleModalFeed}
          onOk={this.exportToFeed}
          confirmLoading={loadingModalFeed}
          onCancel={this.cancelExportFeed}
        >
          <Form onSubmit={this.handleSubmit}>
            <Form.Item
              className="v-center"
              label="Publish Date"
            >
              <DatePicker onChange={this.handleChangeDate} value={publishDate} />
            </Form.Item>
          </Form>
        </Modal>
      </div>
    );
  }
}

export default ListDichotomy;
