import React from 'react';
import PropTypes from 'prop-types';
import {
  Card,
  Col,
  Row,
  Spin,
  Button
} from 'antd';
import { Link } from 'react-router-dom';
import axios from '../../../utils/axios';
import config from '../../../../config';

class DetailDichotomy extends React.Component {
  state = {
    loading: true,
    data: {},
    groups: []
  }

  static propTypes = {
    match: PropTypes.instanceOf(Object)
  }

  componentWillMount() {
    this.getDetailDichotomy();
  }

  getDetailDichotomy = () => {
    const { match } = this.props;
    const { params: { id } } = match;
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}content/dichotomy/${id}`;
    axios({
      method: 'get',
      url: apiPath,
      headers: { Authorization: token }
    }).then((response) => {
      const { data } = response;
      this.setState({
        data: data.result,
        groups: data.result.groups,
        loading: false
      });
    })
    .catch((err) => {
      console.log('eror', err); /* eslint-disable-line no-console */
    });
  }

  render() {
    const {
    loading,
    data,
    groups
  } = this.state;

    const loadingView = (
      <div><center><Spin /></center></div>
    );

    const view = (
      <div>
        <div className="iso__detail">
          <div className="iso__detail-heading"><p className="iso-heading-label">Detail Dichotomy</p></div>
          <div
            className="iso__detail-info-wrapper"
          >
            <div
              style={{ padding: '24px 0' }}
              className="profile__wrapper--img-wrapper"
            >
              {
                data.image !== 'no-image'
                ? <img alt="Featured" src={data.image} className="profile-img" />
                : <img alt="Featured" src="https://via.placeholder.com/60x60/" className="profile-img" />
              }
            </div>
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Game Id</p>
            <p className="iso-info-detail">{data.game_id}</p>
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Name</p>
            <p className="iso-info-detail">{data.name}</p>
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Description</p>
            <p className="iso-info-detail">{data.description}</p>
          </div>
          <div
            className="iso__detail-info-wrapper"
            style={{ display: 'flex' }}
          >
            <p className="iso-info-label" style={{ paddingTop: 42, paddingBottom: 42 }}>Groups</p>
            { groups.map((group, index) => (
              <div className="iso__detail-info-wrapper-two-line" key={index}>
                <Row gutter={24}>
                  <Col span={24}>
                    <Card title="Group Name" style={{ paddingTop: 0 }} bordered={false}>{group.name}</Card>
                  </Col>
                </Row>
              </div>
              ))
            }
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Status</p>
            {
              data.status !== 0 ? <p className="iso-info-detail">active</p>
              : <p className="iso-info-detail">non active</p>
            }
          </div>
          <div
            className="iso__detail-info-wrapper"
          >
            <p className="iso-info-label">Sticky</p>
            {
              data.is_sticky !== 0 ? <p className="iso-info-detail">true</p>
                : <p className="iso-info-detail">false</p>
            }
          </div>
        </div>
        <Link
          to={{ pathname: '/list-dichotomy' }}
          title="back"
        >
          <Button htmlType="button" className="btn-back">Back</Button>
        </Link>
      </div>
    );

    return (
      <div>
        { loading ? loadingView : view }
      </div>
    );
  }
}

export default DetailDichotomy;
