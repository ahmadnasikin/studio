import React from 'react';
import {
  Input, Table, Icon, Divider, Modal, message, Form, DatePicker, Spin, Radio, Button, Row, Col, Alert, Tooltip
} from 'antd';
import moment from 'moment';
import momentTZ from 'moment-timezone';
import axios from '../../../utils/axios';
import { formItemLayout } from '../../../styles/component/formVariable';
import config from '../../../../config';

const { Search } = Input;
const { confirm } = Modal;
let idPublishFeed = null;
class ListFeeds extends React.Component {
  state = {
    data: [],
    pagination: {
      total: 0,
      current: 1,
      pages: 1,
      limit: 10
    },
    sort: null,
    order: 'ASC',
    loading: false,
    searchKey: '',
    visibleModalFeed: false,
    loadingSaveFeed: false,
    loadingModalFeed: false,
    publishDate: moment(),
    statusFeed: 0,
    visibleModalLeadBrd: false,
    loadingModalLeadBrd: false,
    leaderboardData: []
  }

  componentDidMount() {
    this.fetch();
  }

  handleTableChange = (pagination, filter, sorter) => {
    const { pagination: pageState } = this.state;
    const pager = {
      ...pageState,
      current: pagination.current
    };
    const order = sorter.order === 'ascend' ? 'ASC' : 'DESC';
    this.setState({
      pagination: pager,
      sort: sorter.field,
      order
    },
    () => {
      this.fetch();
    });
  }

  _handleKeyPress = (e) => {
    if (e.key === 'Enter') {
      this.fetch();
    }
  }

  handleSearch = (e) => {
    e.preventDefault();
    this.setState({ searchKey: e.target.value });
  }

  fetch = () => {
    const {
      pagination: { current, limit }, searchKey, sort, order
    } = this.state;
    const searchTerm = searchKey !== '' ? searchKey : null;
    const params = {
      page: current,
      limit,
      search_term: searchTerm,
      sort,
      order
    };
    this.setState({ loading: true });
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}content/feed/feeds-portal`;
    axios({
      method: 'get',
      url: apiPath,
      params: {
        ...params
      },
      headers: { Authorization: token }
    }).then((response) => {
      let { pagination } = this.state;
      const {
        data: {
          result: {
            data: {
              total, pages, limit: limitData, docs
            }
          }
        }
      } = response;
      pagination = {
        ...pagination,
        total,
        pages,
        limit: limitData
      };
      const feeds = docs.map(({
        _id: key,
        type,
        dichotomy,
        instagram,
        status: statusNum,
        created_at: created,
        publish_at: published,
        updated_at: updated
      }) => {
        const name = dichotomy !== null ? dichotomy.name : instagram.caption;
        const dichoID = dichotomy !== null ? dichotomy._id : null;
        const status = statusNum === 1 ? 'Active' : 'Non Active';
        const createdAt = momentTZ(created).tz('Asia/Jakarta').format('YYYY-MM-DD');
        const publishAt = momentTZ(published).tz('Asia/Jakarta').format('YYYY-MM-DD');
        const updatedAt = updated === null ? '-' : momentTZ(updated).tz('Asia/Jakarta').format('YYYY-MM-DD');
        return {
          key,
          type,
          dichoID,
          name,
          created_at: createdAt,
          publish_at: publishAt,
          updated_at: updatedAt,
          status,
          statusNum
        };
      });
      this.setState({
        loading: false,
        data: feeds,
        pagination
      });
    }).catch((err) => {
      console.log(err); /* eslint-disable-line no-console */
    });
  }

  deleteFeed = (id) => {
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}content/feed/${id}/delete`;
    axios({
      method: 'post',
      url: apiPath,
      headers: { Authorization: token }
    }).then(() => {
      this.success();
      this.fetch();
    }).catch((err) => {
      this.error();
      console.log(err); /* eslint-disable-line no-console */
    });
  }

  showDeleteConfirm = (id) => {
    const { deleteFeed } = this;
    confirm({
      title: 'Are you sure delete this feed?',
      okText: 'Yes',
      okType: 'danger',
      cancelText: 'No',
      onOk() {
        deleteFeed(id);
      },
      onCancel: () => {}
    });
  }

  showModalFeed = ({ id, publishDate, status: statusFeed }) => {
    idPublishFeed = id;
    const date = moment(publishDate, 'YYYY-MM-DD');
    this.setState({
      publishDate: date,
      visibleModalFeed: true,
      statusFeed
    });
  }

  showModalLeadBrd = ({ id }) => {
    this.setState({
      visibleModalLeadBrd: true,
      loadingModalLeadBrd: true
    });

    this.getLeadBrdById(id)
      .then(({ result: { leaderboard } }) => {
        this.setState({
          leaderboardData: leaderboard,
          loadingModalLeadBrd: false
        });
      });
  }

  getLeadBrdById = (id) => {
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const apiPath = `${config.apiUrl}content/dichotomy/leaderboard/${id}`;
    return axios({
      method: 'get',
      url: apiPath,
      headers: { Authorization: token }
    }).then(response => response.data);
  }

  cancelExportFeed = () => {
    this.setState({
      visibleModalFeed: false
    });
  }

  closeModalLeadBrd = () => {
    this.setState({
      visibleModalLeadBrd: false
    });
  }

  handleChangeDate = (e) => {
    this.setState({
      publishDate: e
    });
  }

  handleChangeStatus = (e) => {
    this.setState({
      statusFeed: e.target.value
    });
  }

  editFeed = () => {
    const {
      state, editToFeed
    } = this;
    const { publishDate: date, statusFeed } = state;
    this.setState({
      loadingModalFeed: true
    });

    const publishDate = moment(date, 'YYYY-MM-DD');
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { username } = user;
    const dataFeed = {
      status: statusFeed,
      created_by: username,
      publish_at: publishDate
    };

    editToFeed(idPublishFeed, dataFeed).then((response) => {
      this.setState({
        loadingModalFeed: false,
        visibleModalFeed: false
      });
      this.fetch();
    });
  }

  editToFeed = (id, data) => {
    const user = JSON.parse(window.localStorage.getItem('user'));
    const { token } = user;
    const url = `${config.apiUrl}content/feed/${id}/update`;
    return axios({
      method: 'post',
      url,
      data,
      headers: { Authorization: token }
    });
  }

  success = () => {
    message.success('success delete dichotomy');
  };

  error = () => {
    message.error('failed delete dichotomy');
  };

  render() {
    const delStyle = {
      cursor: 'pointer',
      color: 'red'
    };
    const editStyle = {
      cursor: 'pointer',
      color: '#1890ff'
    };
    const leadStyle = {
      cursor: 'pointer',
      color: 'green'
    };
    const {
      data,
      pagination,
      loading,
      searchKey,
      visibleModalFeed,
      loadingModalFeed,
      loadingSaveFeed,
      publishDate,
      statusFeed,
      visibleModalLeadBrd,
      loadingModalLeadBrd,
      leaderboardData
    } = this.state;
    const columns = [{
      title: 'Type',
      dataIndex: 'type',
      sorter: true
    }, {
      title: 'Name',
      dataIndex: 'name',
      sorter: false,
      render: (text, record) => (
        <Tooltip placement="right" title={text}>
          {`${text.slice(0, 12)}...`}
        </Tooltip>
      )
    }, {
      title: 'Status',
      dataIndex: 'status',
      sorter: true
    }, {
      title: 'Create At',
      dataIndex: 'created_at',
      sorter: true
    }, {
      title: 'Publish At',
      dataIndex: 'publish_at',
      sorter: true
    }, {
      title: 'Update At',
      dataIndex: 'updated_at',
      sorter: true
    }, {
      title: 'Action',
      dataIndex: 'action',
      render: (text, record) => (
        <span>
          { record.type === 'DICHOTOMY' &&
            <span
              style={leadStyle}
              title="Leaderboard"
              role="presentation"
              tabIndex="-1"
              onClick={() => this.showModalLeadBrd({
                id: record.dichoID
              })}
            >
              <Icon type="crown" />
              <Divider type="vertical" />
            </span>
          }
          <span
            style={editStyle}
            title="Edit"
            role="presentation"
            tabIndex="-1"
            onClick={() => this.showModalFeed({
              id: record.key,
              publishDate: record.publish_at,
              status: record.statusNum
            })}
          >
            <Icon type="edit" />
            <Divider type="vertical" />
          </span>
          <span
            style={delStyle}
            title="Delete"
            role="presentation"
            tabIndex="-1"
            onClick={() => this.showDeleteConfirm(record.key)}
          >
            <Icon type="delete" />
          </span>
        </span>
      )
    }];
    return (
      <div>
        <h1>List Feed</h1>
        <div className="btn__wrapper">
          <div className="btn__wrapper--left">
            <Search
              placeholder="Input search text"
              className="search-wrapper"
              size="small"
              style={{ height: 30 }}
              onChange={this.handleSearch}
              onKeyPress={this._handleKeyPress}
              value={searchKey}
            />
          </div>
        </div>
        <div className="table-wrapper">
          <Table
            className="table"
            columns={columns}
            dataSource={data}
            pagination={pagination}
            loading={loading}
            onChange={this.handleTableChange}
          />
        </div>
        <Modal
          title="Edit Feed"
          visible={visibleModalFeed}
          onOk={this.editFeed}
          confirmLoading={loadingSaveFeed}
          onCancel={this.cancelExportFeed}
        >
          {loadingModalFeed ?
            <div style={{ textAlign: 'center' }}><Spin /></div> :
            <Form onSubmit={this.editFeed}>
              <Form.Item
                className="v-center"
                label="Status"
                {...formItemLayout}
              >
                <Radio.Group onChange={this.handleChangeStatus} value={statusFeed}>
                  <Radio.Button value={0}>Non Active</Radio.Button>
                  <Radio.Button value={1}>Active</Radio.Button>
                </Radio.Group>
              </Form.Item>
              <Form.Item
                className="v-center"
                label="Publish Date"
                {...formItemLayout}
              >
                <DatePicker onChange={this.handleChangeDate} value={publishDate} />
              </Form.Item>
            </Form>
          }
        </Modal>
        <Modal
          title="Leaderboard"
          visible={visibleModalLeadBrd}
          onOk={this.closeModalLeadBrd}
          onCancel={this.closeModalLeadBrd}
          width="80%"
          footer={[
            <Button key="submit" type="primary" onClick={this.closeModalLeadBrd}>
              OK
            </Button>
          ]}
        >
          {loadingModalLeadBrd ?
            <div style={{ textAlign: 'center' }}><Spin /></div> :
            <Row style={{ padding: '0 12px' }}>
              {
                leaderboardData.length < 1 ?
                  <div>
                    <Alert style={{ textAlign: 'center' }} message="Leaderboard not found" type="error" />
                  </div> :
                  <React.Fragment>
                    {leaderboardData.map((value, index) => {
                      const borderStyle = {
                        border: '1px solid rgb(186, 186, 186)',
                        padding: '6px'
                      };
                      const headerStyle = {
                        border: '1px solid rgb(186, 186, 186)',
                        padding: '6px',
                        backgroundColor: '#eee'
                      };
                      const textCenter = {
                        margin: '0',
                        textAlign: 'center'
                      };
                      const noMargin = {
                        margin: '0'
                      };
                      return (
                        <Col span={12} key={index}>
                          <div style={{ marginBottom: 6 }}>
                            <p style={{ fontWeight: 'bold' }}>{value.name}</p>
                            <p>{`Total Skor: ${value.total_score}`}</p>
                          </div>
                          <div>
                            <div>
                              <Row gutter={0}>
                                <Col span={2} style={headerStyle}>
                                  <p style={textCenter}>#</p>
                                </Col>
                                <Col span={10} style={headerStyle}>
                                  <p style={noMargin}>Nama</p>
                                </Col>
                                <Col span={10} style={headerStyle}>
                                  <p style={noMargin}>Skor Tertinggi</p>
                                </Col>
                              </Row>
                            </div>
                            {value.scores.map((dataScore, i) => {
                              return (
                                <div key={i}>
                                  <Row gutter={0}>
                                    <Col span={2} style={borderStyle}>
                                      <p style={textCenter}>{i + 1}</p>
                                    </Col>
                                    <Col span={10} style={borderStyle}>
                                      <p style={noMargin}>{dataScore.subscriber_name}</p>
                                    </Col>
                                    <Col span={10} style={borderStyle}>
                                      <p style={noMargin}>{dataScore.max_score}</p>
                                    </Col>
                                  </Row>
                                </div>
                              );
                            })
                            }
                          </div>
                        </Col>
                      );
                    })}
                  </React.Fragment>
              }
            </Row>
          }
        </Modal>
      </div>
    );
  }
}

export default ListFeeds;
