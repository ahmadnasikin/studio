import {
  Home,
  Logout,
  CreateDichotomy,
  ChangePassword,
  ProfileDetail,
  ListDichotomy,
  DetailDichotomy,
  UpdateDichotomy,
  ListFeeds,
  CreatePromo,
  UpdatePromo,
  Promo,
  DetailPromo
} from './pages';

const routes = [
  {
    id: 'home',
    icon: 'home',
    title: 'Home',
    path: '/',
    component: Home
  }, {
    id: 'dichotomy',
    icon: 'profile',
    title: 'Dichotomy',
    child: [{
      id: 1,
      hideSidebar: true,
      title: 'Create Dichotomy',
      path: '/create-dichotomy',
      component: CreateDichotomy
    }, {
      id: 2,
      icon: 'exception',
      title: 'List Dichotomy',
      path: '/list-dichotomy',
      component: ListDichotomy
    }]
  }, {
    id: 'feed',
    icon: 'switcher',
    title: 'Feed',
    child: [{
      id: 4,
      icon: 'gift',
      title: 'List Feed',
      path: '/list-feed',
      component: ListFeeds
    }]
  },
  // {
  //   id: 5,
  //   title: 'My Profile',
  //   show_headers: true,
  //   hideSidebar: true,
  //   path: '/my-profile',
  //   component: ProfileDetail
  // },
  // {
  //   id: 6,
  //   title: 'Change Password',
  //   show_headers: true,
  //   hideSidebar: true,
  //   path: '/change-password',
  //   component: ChangePassword
  // },
  {
    id: 7,
    title: 'Logout',
    show_headers: true,
    hideSidebar: true,
    path: '/logout',
    component: Logout
  }, {
    hideSidebar: true,
    title: 'Dichotomy Detail',
    path: '/content/dichotomy/detail/:id/',
    component: DetailDichotomy
  }, {
    hideSidebar: true,
    title: 'Dichotomy Update',
    path: '/content/dichotomy/edit/:id/',
    component: UpdateDichotomy
  }, {
    id: 'Promo Mangement',
    icon: 'tags',
    title: 'Promo Management',
    child: [{
      id: 10,
      icon: 'credit-card',
      title: 'List Promo',
      path: '/promo',
      component: Promo
    }, {
      hideSidebar: true,
      title: 'Create Promo',
      path: '/promo/create-promo',
      component: CreatePromo
    }, {
      hideSidebar: true,
      title: 'Update Promo',
      path: '/content/promo/edit/:id',
      component: UpdatePromo
    }, {
      hideSidebar: true,
      title: 'Detail Promo',
      path: '/content/promo/detail/:id',
      component: DetailPromo
    }]
  }
];

export default routes;
