module.exports = {
  gameList: {
    Random: {
      index: null
    },
    flappyChicken: {
      index: 1,
      path: 'flappy-chicken/',
      winScore: 1000
    },
    exactMath: {
      index: 2,
      path: 'exact-number/',
      winScore: 1000
    },
    comboButton: {
      index: 3,
      path: 'combo-button/',
      winScore: 1000
    },
    matchCard: {
      index: 4,
      path: 'match-pict/',
      winScore: 1000
    },
    jankenpon: {
      index: 5,
      path: 'rock-paper-scissor/',
      winScore: 1000
    },
    stack: {
      index: 6,
      path: 'stack-totem/',
      winScore: 1000
    },
    hungryCat: {
      index: 7,
      path: 'hungry-cat/',
      winScore: 1000
    },
    guessTheCoin: {
      index: 8,
      path: 'guess-coin/',
      winScore: 100
    },
    beanstalk: {
      index: 9,
      path: 'bean-stalk/',
      winScore: 1000
    },
    froggit: {
      index: 10,
      path: 'froggit/',
      winScore: 1000
    },
    crazyCar: {
      index: 11,
      path: 'crazy-car/',
      winScore: 1500
    },
    appleTree: {
      index: 12,
      path: 'apple-tree/'
    },
    boxTower: {
      index: 13,
      path: 'box-tower/',
      winScore: 800
    }
  }
};
