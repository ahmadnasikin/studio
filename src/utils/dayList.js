module.exports = {
  days: {
    Sunday: {
      index: 1
    },
    Monday: {
      index: 2
    },
    Tuesday: {
      index: 3
    },
    Wednesday: {
      index: 4
    },
    Thursday: {
      index: 5
    },
    Friday: {
      index: 6
    },
    Saturday: {
      index: 7
    }
  }
};
