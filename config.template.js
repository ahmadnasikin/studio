const config = {
  apiUrl: '__SERVICE_API__',
  apikey: '__API_KEY_GATEWAY__',

  // cloudinary account
  contentApiKey: '__CLOUDINARY__CONTENT__API__KEY__UPLOAD',
  cloudinaryUploadUrl: '__CLOUDINARY__URL__UPLOAD',
  cloudinaryApiKey: 'CLOUDINAR__API__KEY',
  cloudinaryApiSecret: 'CLOUDINARY__API__SECRET',
  cloudinaryCloudName: 'CLOUDINARY_CLOUD_NAME',
  cloudinaryUploadPreset: 'CLOUDINARY_UPLOAD_PRESET'
};

export default config;
